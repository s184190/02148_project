package engine.input;

import java.awt.Component;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

public class MouseHandler {

	public boolean isPressed[];
	public boolean isClicked[];
	public boolean isReleased[];

	public int x, y;
	
	private int numberOfButtons = 3;

	private MouseListener mouseListener;
	private MouseMotionListener mouseMotionListener;
	
	private ArrayList<MouseEvent> preMouses;
	private ArrayList<MouseEvent> pendingMouses;
	
	private Component component;
	
	public MouseHandler() {
		isPressed = new boolean[numberOfButtons];
		isClicked = new boolean[numberOfButtons];
		isReleased = new boolean[numberOfButtons];

		preMouses = new ArrayList<>();
		pendingMouses = new ArrayList<>();
		
		mouseListener = new MouseListener() {
			@Override
			public void mouseReleased(MouseEvent e) {
				preMouses.add(e);
			}

			@Override
			public void mousePressed(MouseEvent e) {
				preMouses.add(e);
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				preMouses.add(e);
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}
		};
		
		mouseMotionListener = new MouseMotionListener() {
			@Override
			public void mouseMoved(MouseEvent e) {
				preMouses.add(e);
			}
			
			@Override
			public void mouseDragged(MouseEvent e) {
			}
		};
	}
	
	public void attach(Component newComponent) {
		if (component != null) {
			component.removeMouseListener(mouseListener);
			component.removeMouseMotionListener(mouseMotionListener);
		}
		
		newComponent.addMouseListener(mouseListener);
		newComponent.addMouseMotionListener(mouseMotionListener);
		
		component = newComponent;
	}
	
	public void act() {
		reset();
		
		pendingMouses.addAll(preMouses);

		for (MouseEvent event : pendingMouses) {
			int eventId = event.getID();
			if (eventId == MouseEvent.MOUSE_CLICKED) {
				int button = getButtonType(event);
				isClicked[button] = true;
			} else if (eventId == MouseEvent.MOUSE_PRESSED) {
				int button = getButtonType(event);
				isPressed[button] = true;
			} else if (eventId == MouseEvent.MOUSE_RELEASED) {
				int button = getButtonType(event);
				isReleased[button] = true;
				isPressed[button] = false;
			} else if (eventId == MouseEvent.MOUSE_MOVED) {
				Point position = event.getPoint();
				x = position.x;
				y = position.y;
			} else {
				throw new IllegalStateException("we should not get other events!");
			}
		}
		
		preMouses.removeAll(pendingMouses);
		pendingMouses.clear();
	}

	private int getButtonType(MouseEvent e) {
		if (e.getButton() == MouseEvent.BUTTON1) {
			return Button.LEFT;
		} else if (e.getButton() == MouseEvent.BUTTON2) { // @test
			return Button.MIDDLE;
		} else if (e.getButton() == MouseEvent.BUTTON3) {
			return Button.RIGHT;
		}

		// throw new IllegalStateException("not supported: " + e.getButton());'
		return Button.UNKNOWN;
		// Button is non of the above, when mouse moved or if
		// a mouse has extra/special buttons.
	}

	public void reset() {
		for (int i = 0; i < numberOfButtons; i++) {
			isReleased[i] = false;
		}

		for (int i = 0; i < numberOfButtons; i++) {
			isClicked[i] = false;
		}
	}
}

/*
 * public boolean isAnyPressed() { return isPressed(Button.LEFT) ||
 * isPressed(Button.MIDDLE) || isPressed(Button.RIGHT); }
 * 
 * public boolean isAnyClicked() { return isClicked(Button.LEFT) ||
 * isClicked(Button.MIDDLE) || isClicked(Button.RIGHT); }
 * 
 * public boolean isAnyReleased() { return isReleased(Button.LEFT) ||
 * isReleased(Button.MIDDLE) || isReleased(Button.RIGHT); }
 */
