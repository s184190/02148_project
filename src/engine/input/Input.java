package engine.input;

public interface Input {
	
	public int getX();
	public int getY();
	
	// @todo: isAnyPressed?
	public boolean isMouseReleased(int button);
	public boolean isMousePressed(int button);
	public boolean isMouseClicked(int button);
	
	public boolean isKeyJustPressed(int keyCode);
	public boolean isKeyPressed(int keyCode);
	public boolean isKeyReleased(int keyCode);
	public void releaseAllKeys();

}
