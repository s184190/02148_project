package engine.input;

public class InputHandler implements Input {
	
	private MouseHandler mouseHandler;
	private KeyboardHandler keyboardHandler;
	
	public InputHandler(MouseHandler mouseHandler, KeyboardHandler keyboardHandler) {
		this.mouseHandler = mouseHandler;
		this.keyboardHandler = keyboardHandler;
	}

	@Override
	public int getX() {
		return mouseHandler.x;
	}

	@Override
	public int getY() {
		return mouseHandler.y;
	}

	@Override
	public boolean isMouseReleased(int button) {
		return mouseHandler.isReleased[button];
	}

	@Override
	public boolean isMousePressed(int button) {
		return mouseHandler.isPressed[button];
	}

	@Override
	public boolean isMouseClicked(int button) {
		return mouseHandler.isClicked[button];
	}

	@Override
	public boolean isKeyJustPressed(int keyCode) {
		return keyboardHandler.isJustPressed[keyCode];
	}

	@Override
	public boolean isKeyPressed(int keyCode) {
		return keyboardHandler.isPressed[keyCode];
	}

	@Override
	public boolean isKeyReleased(int keyCode) {
		return keyboardHandler.isJustReleased[keyCode];
	}
	
	@Override
	public void releaseAllKeys() {
		for(int i = 0; i < Keys.numberOfKeys; i++) {
			keyboardHandler.isPressed[i] = false;
		}
	}

}
