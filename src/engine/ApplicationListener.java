package engine;

import engine.input.Input;

public class ApplicationListener {
	public Input input;
	public Application app;
	
	public void init() {}
	public void update() {}
	public void draw() {}
	public void dispose() {}
	public void resize(int width, int height) {}
}
