package engine.graphics;

import engine.math.Vector2;

public interface Viewport {
	public void update(float screenWidth, float screenHeight);
	public Vector2 worldToScreen(float x, float y);
	public Vector2 screenToWorld(int x, int y);
	public float getCameraZoom();
	public float getScaleX();
	public float getScaleY();
	public boolean flipped();
}

