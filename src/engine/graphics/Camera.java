package engine.graphics;

import java.util.ArrayList;
import java.util.List;

import engine.math.Vector2;

public class Camera {

	public float x, y;
	public float tx, ty;
	
	public float zoom;
	public float targetZoom;

	public float moveSpeed;
	public float zoomSpeed;

	public Camera() {
		moveSpeed = .08f;
		zoomSpeed = .02f;

		zoom = 1;
		targetZoom = 1;
	}

	public void lookAtNow(float x, float y) {
		this.x = x;
		this.y = y;
		this.tx = x;
		this.ty = y;
	}

	public void lookAt(float tx, float ty) {
		this.tx = tx;
		this.ty = ty;
	}

	public void update() {
		if (points.size() > 0) {
			shakeTicks += 1;
			
			if (shakeTicks >= maxShakeTicks) {
				shakeTicks = 0;
				Vector2 position = points.remove(0);
				lookAt(position.x, position.y);
			}
		}
		
		float dx = tx - x;
		float dy = ty - y;

		x += dx * moveSpeed;
		y += dy * moveSpeed;

		float dzoom = targetZoom - zoom;
		zoom += dzoom * zoomSpeed;
	}

	private List<Vector2> points = new ArrayList<>();
	
	private int shakeTicks;
	private int maxShakeTicks;

	public void shake(float numberOfShakes, float shakeStrength, int timePerShake) {
		maxShakeTicks = timePerShake;
		
		for (int i = 0; i < numberOfShakes; i++) {
			points.add(generatePoint2(shakeStrength));
		}

		points.add(new Vector2(x, y));
	}

	private Vector2 generatePoint2(float scalar) {
		float theta = random(0, 2 * Math.PI);
		Vector2 unit = new Vector2(1, 0);
		unit.rotate(theta).scl(scalar);
		
		return unit.add(x, y);
	}
	
	private Vector2 generatePoint(float scalar) {
		float dx = random(-1, 1) * scalar;
		float dy = random(-1, 1) * scalar;
		return new Vector2(x + dx, y + dy);
	}

	private float random(double min, double max) {
		return (float) (min + (Math.random() * (max - min)));
	}

	public void zoomNow(float zoom) {
		this.zoom = zoom;
		this.targetZoom = zoom;
	}

	public void zoom(float targetZoom) {
		this.targetZoom = targetZoom;
	}
}
