package engine.graphics;

import engine.math.Vector2;

public class StretchedViewport implements Viewport {

	private float virtualWidth, virtualHeight;
	private float scaleX, scaleY;
	
	private float screenHeight;

	private Camera camera;
	
	private boolean flipped;

	public StretchedViewport(float virtualWidth, float virtualHeight) {
		this(virtualWidth, virtualHeight, false);
	}
	
	public StretchedViewport(float virtualWidth, float virtualHeight, boolean flipped) {
		this.virtualWidth = virtualWidth;
		this.virtualHeight = virtualHeight;
		this.flipped = flipped;

		tmp = new Vector2();
	}

	public void setCamera(Camera camera) {
		this.camera = camera;
	}

	public void update(float screenWidth, float screenHeight) {
		this.screenHeight = screenHeight;
		scaleX = screenWidth / virtualWidth;
		scaleY = screenHeight / virtualHeight;
	}

	private final Vector2 tmp;

	public Vector2 worldToScreen(float x, float y) {
		float centerX = virtualWidth / 2.0f;
		float centerY = virtualHeight / 2.0f;

		// to screen (if there was no scaling from the viewport to fit the screen)
		float screenX = (x - camera.x) * camera.zoom + centerX;
		float screenY = (y - camera.y) * camera.zoom + centerY;

		// scale to fit the screen
		float scaledX = screenX * scaleX;
		float scaledY = screenY * scaleY;
		
		if (flipped) scaledY = screenHeight - scaledY;

		return tmp.set(scaledX, scaledY);
	}

	public float getCameraZoom() {
		return camera.zoom;
	}

	public Vector2 screenToWorld(int x, int y) {
		float centerX = virtualWidth / 2.0f;
		float centerY = virtualHeight / 2.0f;

		float worldX = (x - centerX) / camera.zoom + camera.x;
		float worldY = (y - centerY) / camera.zoom + camera.y;

		float unscaledX = worldX / scaleX;
		float unscaledY = worldY / scaleY;

		return tmp.set(unscaledX, unscaledY);
	}

	@Override
	public float getScaleX() {
		return scaleX;
	}

	@Override
	public float getScaleY() {
		return scaleY;
	}

	@Override
	public boolean flipped() {
		return flipped;
	}
}
