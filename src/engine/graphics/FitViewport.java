package engine.graphics;

import engine.math.Vector2;

public class FitViewport implements Viewport {

	private float virtualWidth, virtualHeight;
	private float scale;
	
	private float screenHeight;
	private float screenWidth;

	private Camera camera;
	
	private boolean flipped;
	
	public FitViewport(float virtualWidth, float virtualHeight) {
		this(virtualWidth, virtualHeight, false);
	}

	public FitViewport(float virtualWidth, float virtualHeight, boolean flip) {
		this.virtualWidth = virtualWidth;
		this.virtualHeight = virtualHeight;

		flipped = flip;
		
		tmp = new Vector2();
	}

	public void setCamera(Camera camera) {
		this.camera = camera;
	}

	public void update(float screenWidth, float screenHeight) {
		this.screenWidth = screenWidth;
		this.screenHeight = screenHeight;
		
		float ratioX = screenWidth / virtualWidth;
		float ratioY = screenHeight / virtualHeight;
		float ratio = Math.min(ratioX, ratioY);
		this.scale = ratio;
	}

	private final Vector2 tmp;

	public Vector2 worldToScreen(float x, float y) {
		float centerX = virtualWidth / 2.0f;
		float centerY = virtualHeight / 2.0f;
		
		// to screen (if there was no scaling from the viewport to fit the screen)
		float screenX = (x - camera.x) * camera.zoom + centerX;
		float screenY = (y - camera.y) * camera.zoom + centerY;

		// scale to fit the screen
		float scaledX = screenX * scale;
		float scaledY = screenY * scale;
		
		if (flipped) scaledY = screenHeight - scaledY;
		
		return tmp.set(scaledX, scaledY);
	}

	public float getCameraZoom() {
		return camera.zoom;
	}

	public Vector2 screenToWorld(int x, int y) {
		float centerX = virtualWidth / 2.0f;
		float centerY = virtualHeight / 2.0f;

		float worldX = (x - centerX) / camera.zoom + camera.x;
		float worldY = (y - centerY) / camera.zoom + camera.y;

		float unscaledX = worldX / scale;
		float unscaledY = worldY / scale;

		return tmp.set(unscaledX, unscaledY);
	}

	@Override
	public float getScaleX() {
		return scale;
	}

	@Override
	public float getScaleY() {
		return scale;
	}
	
	@Override
	public boolean flipped() {
		return flipped;
	}
}
