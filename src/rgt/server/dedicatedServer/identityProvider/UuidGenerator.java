package rgt.server.dedicatedServer.identityProvider;

public class UuidGenerator {
	
	private int nextUuid;
	
	public UuidGenerator() {
		nextUuid = 0;
	}

	public int getUuid() {
		int uuid = nextUuid;
		nextUuid += 1;
		return uuid;
	}
}
