package rgt.server.dedicatedServer.identityProvider;

public class IdentityInfo {
	public int uid;
	public String username;
	public String password;

	public boolean alive;	// @redundant?
	public long msLastSeenAlive;
	public boolean requestedAliveUpdate;
}
