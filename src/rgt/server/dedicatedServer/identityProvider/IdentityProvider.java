package rgt.server.dedicatedServer.identityProvider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.jspace.ActualField;
import org.jspace.FormalField;
import org.jspace.Space;
import org.jspace.SpaceRepository;

import rgt.server.FixedLoopProcess;
import rgt.shared.Actors;
import rgt.shared.NetworkUtil;
import rgt.shared.protocols.alive.AliveMessageTypes;
import rgt.shared.protocols.authentication.AuthenticationMessage;
import rgt.shared.protocols.authentication.Credentials;
import rgt.shared.protocols.authentication.DisconnectMessage;
import rgt.shared.protocols.authentication.signIn.SignInServerPacket;
import rgt.shared.protocols.authentication.signIn.SignInStatus;
import rgt.shared.protocols.authentication.signUp.SignUpServerPacket;
import rgt.shared.protocols.authentication.signUp.SignUpStatus;

public class IdentityProvider {

	private UuidGenerator uidGenerator;

	private Space aliveSpace;
	private Space authSafeSpace;
	
	private int minPasswordLength;
	private int minUsernameLength;
	
	private Map<Credentials, Integer> credentialsToUid;
	private Map<Integer, IdentityInfo> uidToIdentitiyInfo;
	private List<String> usernamesTaken;
	
	private IdentityProvidierListener listener;
	
	private int maxTimeSinceLastSeenAlive;
	private int requestAliveTime;
	
	public IdentityProvider(SpaceRepository repository) {
		aliveSpace = repository.get(NetworkUtil.aliveSpaceId);
		authSafeSpace = repository.get(NetworkUtil.authSafeId);
		
		uidGenerator = new UuidGenerator();
		
		minPasswordLength = 6;
		minUsernameLength = 1;
		
		maxTimeSinceLastSeenAlive = 5_000; // ms
		requestAliveTime = 2_000; // ms
		
		credentialsToUid = new HashMap<>();
		uidToIdentitiyInfo = new HashMap<>();
		usernamesTaken = new ArrayList<>();
	}
	
	public void setListener(IdentityProvidierListener listener) {
		this.listener = listener;
	}
	
	public void run() {
		listenForAuthentication();
		listenForAliveResponses();
		disconnectedClientsHandler();
	}
	
	public void disconnectedClientsHandler() {
		new Thread(() ->  {
			FixedLoopProcess process = new FixedLoopProcess() {
				@Override
				public void update(float dt) {
					// use an iterator so we can safely iterate in-case an identity info is added duing the loop (from another thread).
					for (Iterator<IdentityInfo> it = uidToIdentitiyInfo.values().iterator(); it.hasNext();) {
						IdentityInfo info = (IdentityInfo) it.next();
						if (info.alive) {
							long msSinceLastAlive = getTimeMs() - info.msLastSeenAlive;
							if (msSinceLastAlive >= maxTimeSinceLastSeenAlive) {
								info.requestedAliveUpdate = false;
								info.alive = false;
								
								try {
									authSafeSpace.put(Actors.server, DisconnectMessage.disconnected, info.uid);
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
								
								listener.onChange();
								
							} else if (msSinceLastAlive >= requestAliveTime) {
								if (!info.requestedAliveUpdate) {
									info.requestedAliveUpdate = true;
									
									try {
										aliveSpace.put(AliveMessageTypes.aliveRequest, info.uid);
										
										Object[] aliveTuple = aliveSpace.queryp(
												new ActualField(AliveMessageTypes.aliveRequest), 
												new ActualField(info.uid));
										
									} catch (InterruptedException e) {
										e.printStackTrace();
									}
								}
							}
						}
					}
				}
			};
			
			process.setTps(10);
			process.startLoop();
			
		}).start();
	}

	private void listenForAuthentication() {
		new Thread(() -> {
			while (true) {
				try {
					Object[] authenticationTuple = authSafeSpace.get(
							new ActualField(Actors.identityProvider),
							new FormalField(AuthenticationMessage.class),
							new FormalField(Object.class));
					
					Object packet = authenticationTuple[2];
					
					AuthenticationMessage message = (AuthenticationMessage) authenticationTuple[1];
					switch (message) {
						case signUp: {
							Credentials credentials = (Credentials) packet;
							String username = credentials.username;
							String password = credentials.password;
							
							SignUpStatus status = getSignUpStatus(username, password);
							
							SignUpServerPacket serverPacket = new SignUpServerPacket();
							serverPacket.status = status;
							
							if (status == SignUpStatus.success) {
								usernamesTaken.add(username);

								int uid = uidGenerator.getUuid();
								registerIdentity(credentials, uid);
								
								serverPacket.uid = uid;
								serverPacket.username = username;
							
								listener.onChange();

							}
							
							authSafeSpace.put(credentials, AuthenticationMessage.signUp, serverPacket);

							break;
						}
						
						case signIn: {
							Credentials credentials = (Credentials) packet;
							
							SignInServerPacket response = new SignInServerPacket();
							
							boolean userExists = credentialsToUid.containsKey(credentials);
							if (userExists) {
								int uid = credentialsToUid.get(credentials);
								IdentityInfo info = uidToIdentitiyInfo.get(uid);
								
								if (info.alive) {
									response.status = SignInStatus.alreadyLoggedIn;
								} else {
									info.msLastSeenAlive = getTimeMs();
									info.alive = true;
									
									response.status = SignInStatus.success;
									response.uid = info.uid;
									
									listener.onChange();
								}
							} else {
								response.status = SignInStatus.userNotFound;
							}
							
							authSafeSpace.put(credentials, AuthenticationMessage.signIn, response);	
							
							break;
						}
						
						case signOut: {
							int uid = (int) packet;
							
							IdentityInfo identityInfo = uidToIdentitiyInfo.get(uid);
							identityInfo.alive = false;
							identityInfo.requestedAliveUpdate = false;
							
							authSafeSpace.put(uid, AuthenticationMessage.signOut, uid);

							listener.onChange();
							
							break;
						}
					}
					
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
		}).start();
	}

	private void registerIdentity(Credentials credentials, int uid) {
		IdentityInfo info = new IdentityInfo();
		info.uid = uid;
		info.username = credentials.username;
		info.password = credentials.password;
		info.alive = false;
		credentialsToUid.put(credentials, uid);
		uidToIdentitiyInfo.put(uid, info);
	}
	
	private void listenForAliveResponses() {
		new Thread(() -> {
			
			while (true) {
				try {
					Object[] maybeAliveTuple = aliveSpace.get(
							new ActualField(AliveMessageTypes.aliveResponse), 
							new FormalField(Integer.class));
					
					int uid = (int) maybeAliveTuple[1];
					
					IdentityInfo info = uidToIdentitiyInfo.get(uid);
					if (info.alive) {
						info.msLastSeenAlive = getTimeMs();
						info.requestedAliveUpdate = false;
					}
					
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

			}
		}).start();
	}
	
	private SignUpStatus getSignUpStatus(String username, String password) {
		SignUpStatus status;
		if (!isUsernameAvailable(username)) {
			status = SignUpStatus.unavailableUsername;
		} else if (!isPasswordValid(password)) {
			status = SignUpStatus.weakPassword;
		} else {
			status = SignUpStatus.success;
		}
		return status;
	}

	private boolean isUsernameAvailable(String username) {
		if (username.length() < minUsernameLength) return false;
		return !usernamesTaken.contains(username);
	}

	private boolean isPasswordValid(String password) {
		return password.length() >= minPasswordLength;
	}
	
	private long getTimeMs() {
		return System.currentTimeMillis();
	}
	
	public Collection<IdentityInfo> getIdentityInfos() {
		return uidToIdentitiyInfo.values();
	}
}
