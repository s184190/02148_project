package rgt.server.dedicatedServer.gameServer;

public class Timer {
	
	public float tick;
	public float maxTick;
	
	private boolean shouldTick;
	
	public void set(float maxTick) {
		this.maxTick = maxTick;
	}
	
	public void ready() {
		tick = 0;
		shouldTick = true;
	}
	
	public void passed(float dt) {
		if (shouldTick) {
			tick += dt;
			
			if (tick >= maxTick) {
				shouldTick = false;
				tick = maxTick;
			}
			
		}
	}
	
	public boolean expired() {
		return tick == maxTick;
	}
}
