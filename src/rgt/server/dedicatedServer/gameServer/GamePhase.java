package rgt.server.dedicatedServer.gameServer;

public enum GamePhase {
	starting,
	game,
	ending;
}
