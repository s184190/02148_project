package rgt.server.dedicatedServer.gameServer;

import java.util.ArrayList;
import java.util.List;

public class GameInfo {
	
	public int gid;	// gameServer identifier
	public String gameSpaceId;
	
	public String name;

	public GamePhase phase;
	
	public List<Integer> playerUids = new ArrayList<>();
	
	public long pieceSeed;
	public long debtSeed;
	
	public Timer startTimer = new Timer();
	public Timer endingTimer = new Timer();
	
}
