package rgt.server.dedicatedServer.gameServer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.jspace.ActualField;
import org.jspace.FormalField;
import org.jspace.Space;
import org.jspace.SpaceRepository;

import rgt.client.scenes.GameState;
import rgt.server.FixedLoopProcess;
import rgt.shared.Actors;
import rgt.shared.NetworkUtil;
import rgt.shared.NonExhaustiveSwitchException;
import rgt.shared.protocols.game.ClientGameMessages;
import rgt.shared.protocols.game.GameMetaMessage;
import rgt.shared.protocols.game.ServerGameMessages;
import rgt.tetris.networking.Board;
import rgt.tetris.networking.Tetris;

public class GameServer extends FixedLoopProcess {
	
	private SpaceRepository repository;
	
	private Space gameSpace;
	private Space gameEndingSpace;
	
	private GameInfo gameInfo;
	
	private Tetris tetris;
	private Map<Integer, Integer> uidToBoardIndex;
	private Map<Integer, Integer> boardIndexToUid;
	
	public GameServer(SpaceRepository repository, GameInfo gameInfo) {
		this.repository = repository;
		this.gameInfo = gameInfo;
		
		gameSpace = repository.get(gameInfo.gameSpaceId);
		gameEndingSpace = repository.get(NetworkUtil.gameEndingSpaceId);
		
		setTps(60);

		gameInfo.pieceSeed = getPieceSeed();
		gameInfo.debtSeed = getDebtSeed();
		
		gameInfo.startTimer.set(2f);
		gameInfo.startTimer.ready();
		
		gameInfo.endingTimer.set(10f);	// wait 10 secs before closing the gameserver, such that
																		// the clients have time to fetch the "game ended" message.
		gameInfo.endingTimer.ready();
		
		uidToBoardIndex = new HashMap<>();
		boardIndexToUid = new HashMap<>();

		int numberOfPlayers = gameInfo.playerUids.size();
		for (int boardIndex = 0; boardIndex < numberOfPlayers; boardIndex++) {
			int uid = gameInfo.playerUids.get(boardIndex);
			uidToBoardIndex.put(uid, boardIndex);
			boardIndexToUid.put(boardIndex, uid);
		}
		
		tetris = new Tetris();
		tetris.create(numberOfPlayers, gameInfo.pieceSeed, gameInfo.debtSeed);
	}
	
	@Override
	public void update(float dt) {
		
		switch (gameInfo.phase) {
			case starting: {
				handleStarting(dt);
				break;
			}
			
			case game: {
				handleGame(dt);
				break;
			}
			
			case ending: {
				handleEnding(dt);
				break;
			}
			
			default: 
				throw new NonExhaustiveSwitchException(gameInfo.phase + " not supported!");
		}
	}
	
	private void handleEnding(float dt) {
		gameInfo.endingTimer.passed(dt);
		if (gameInfo.endingTimer.expired()) {
			repository.closeGate(gameInfo.gameSpaceId);
			stop();
		}
	}

	private void handleStarting(float dt) {
		handleMetaMessages();
		
		gameInfo.startTimer.passed(dt);
		if (gameInfo.startTimer.expired()) {
			gameInfo.phase = GamePhase.game;
		
			// ignore packets until the game started.
			// just consume the move requests before changing to game-phase.
			getAllMoveRequests();
		}
	}
	
	private void handleGame(float dt) {
		handleMetaMessages();
		
		List<Object[]> newMoveRequests = getAllMoveRequests();
		for (Object[] moveRequest : newMoveRequests) {
			ClientGameMessages message = (ClientGameMessages) moveRequest[1];
			int uid = (int) moveRequest[2];
			
			int boardIndex = uidToBoardIndex.get(uid);
			Board board = tetris.getBoard(boardIndex);
			
			if (board.dead) continue;
			
			switch (message) {
				case down: {
					tetris.down(board);
					broadcast(boardIndex, ServerGameMessages.down);
					break;
				}
				
				case left: {
					tetris.left(board);
					broadcast(boardIndex, ServerGameMessages.left);
					break;
				}
				
				case right: {
					tetris.right(board);
					broadcast(boardIndex, ServerGameMessages.right);
					break;
				}
				
				case rotate: {
					tetris.rotate(board);
					broadcast(boardIndex, ServerGameMessages.rotate);
					break;
				}
				
				case space: {
					tetris.space(board);
					broadcast(boardIndex, ServerGameMessages.space);
					break;
				}
				case hold: {
					tetris.hold(board);
					broadcast(boardIndex, ServerGameMessages.hold);
					break;
				}
			}
		}
		
		// update boards
		for (Entry<Integer, Integer> entry : uidToBoardIndex.entrySet()) {
			int boardIndex = entry.getValue();
			Board board = tetris.getBoard(boardIndex);

			if (board.dead) continue;
			
			tetris.fallTick(dt, board);
			if (tetris.shouldFall(board)) {
				tetris.fall(board);
				broadcast(boardIndex, ServerGameMessages.fall);
			}
			
			tetris.solidifyTick(dt, board);
			if (tetris.shouldSolidify(board)) {
				tetris.onSolidify(board);

				broadcast(boardIndex, ServerGameMessages.solidify);
			}
		}
		
		if (tetris.gameOver()) {
			List<Board> topThreeBoards = tetris.getTopRankings(3);
			List<Integer> topThreeUids = new ArrayList<>();
			
			for (Board topBoard : topThreeBoards) {
				int boardIndex = tetris.getBoardId(topBoard);
				int uid = boardIndexToUid.get(boardIndex);
				topThreeUids.add(uid);
			}
			
			try {
				// send top 3 uids to server
				gameEndingSpace.put(Actors.server, gameInfo.gid, topThreeUids);

				// wait for ack.
				gameEndingSpace.get(new ActualField(gameInfo.gid));

				broadcast(ServerGameMessages.gameOver);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			gameInfo.phase = GamePhase.ending;

		}
	}
	
	private void handleMetaMessages() {
		List<Object[]> metaRequests = getAllMetaRequests();
		for (Object[] metaRequest : metaRequests) {
			int uid = (int) metaRequest[2];
			
			GameState gameState = new GameState();
			gameState.boards = tetris.getBoards();
			gameState.yourBoardId = uidToBoardIndex.get(uid);
			
			try {
				gameSpace.put(uid, GameMetaMessage.gameState, gameState);

			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private void broadcast(int boardId, ServerGameMessages message) {
		for (int uid : gameInfo.playerUids) {
			try {
				gameSpace.put(uid, message, boardId);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void broadcast(ServerGameMessages message) {
		broadcast(-1, message);
	}

	private List<Object[]> getAllMoveRequests() {
		List<Object[]> tuples = null;
		
		try {
			tuples = gameSpace.getAll(
					new ActualField(Actors.gameServer),
					new FormalField(ClientGameMessages.class),
					new FormalField(Integer.class));	// uid
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		return tuples;
	}
	
	private List<Object[]> getAllMetaRequests() {
		List<Object[]> tuples = null;
		
		try {
			tuples = gameSpace.getAll(
					new ActualField(Actors.gameServer),
					new FormalField(GameMetaMessage.class),
					new FormalField(Object.class));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		return tuples;
	}
	
	private long getPieceSeed() {
		return System.nanoTime();
	}
	
	private long getDebtSeed() {
		return System.nanoTime();
	}
}
