package rgt.server.dedicatedServer.userServer;

import java.util.ArrayList;
import java.util.List;

public class LobbyInfo {
	public int lid;
	public String name;
	public int ownerUid;
	public List<Integer> playerUids = new ArrayList<>();	// inclusive owner
	
	public int maxNumberOfPlayers;
	public int minNumberOfPlayersToStartGame;
}
