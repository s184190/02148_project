package rgt.server.dedicatedServer.userServer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jspace.ActualField;
import org.jspace.FormalField;
import org.jspace.SequentialSpace;
import org.jspace.Space;
import org.jspace.SpaceRepository;

import rgt.encryption.Encryption;
import rgt.server.FixedLoopProcess;
import rgt.server.dedicatedServer.gameServer.GameInfo;
import rgt.server.dedicatedServer.gameServer.GamePhase;
import rgt.server.dedicatedServer.gameServer.GameServer;
import rgt.server.dedicatedServer.identityProvider.UuidGenerator;
import rgt.shared.Actors;
import rgt.shared.NetworkUtil;
import rgt.shared.NonExhaustiveSwitchException;
import rgt.shared.protocols.authentication.AuthenticationMessage;
import rgt.shared.protocols.authentication.Credentials;
import rgt.shared.protocols.authentication.DisconnectMessage;
import rgt.shared.protocols.authentication.signIn.SignInResponsePacket;
import rgt.shared.protocols.authentication.signIn.SignInServerPacket;
import rgt.shared.protocols.authentication.signIn.SignInStatus;
import rgt.shared.protocols.authentication.signUp.SignUpResponsePacket;
import rgt.shared.protocols.authentication.signUp.SignUpServerPacket;
import rgt.shared.protocols.authentication.signUp.SignUpStatus;
import rgt.shared.protocols.user.broadcast.BroadcastMessages;
import rgt.shared.protocols.user.joinLobby.JoinLobbyRequest;
import rgt.shared.protocols.user.joinLobby.JoinLobbyResponse;
import rgt.shared.protocols.user.lobbyList.ClientLobbyInfo;
import rgt.shared.protocols.user.lobbyList.LobbyListResponsePacket;
import rgt.shared.protocols.user.startGame.StartGameResponse;
import rgt.shared.protocols.user.userInfo.UserInfoPacket;
import rgt.shared.protocols.user.userInfo.UserMessages;
import rgt.shared.protocols.users.lobbyCreation.LobbyCreationRequestPacket;
import rgt.shared.protocols.users.lobbyCreation.LobbyCreationResponsePacket;

public class TetrisServer {
	
	private SpaceRepository repository;

	private Space userSpace;
	private Space authenticationSpace;
	private Space authSafeSpace;
	private Space gameEndingSpace;
	
	private ServerListener listener;
	
	private Map<Integer, UserInfo> uidToUserInfo;
	private Map<Integer, LobbyInfo> lidToLobbyInfo;
	private Map<Integer, GameInfo> gidToGameInfo;
	
	private UuidGenerator lidGenerator;
	
	private int minNumberOfPlayersToStartGame;
	private int maxNumberOfPlayersInLobby;
	
	private String privateKey;
	
	private Encryption encryption;
	
	private String ip4;
	
	public TetrisServer(SpaceRepository repository, String ip4) {
		this.repository = repository;
		this.ip4 = ip4;
		
		uidToUserInfo = new HashMap<>();
		lidToLobbyInfo = new HashMap<>();
		gidToGameInfo = new HashMap<>();
		
		encryption = new Encryption();
		
		privateKey = "private key";
		
		userSpace = repository.get(NetworkUtil.tetrisSpaceId);
		authenticationSpace = repository.get(NetworkUtil.authenticationSpaceId);
		authSafeSpace = repository.get(NetworkUtil.authSafeId);
		gameEndingSpace = repository.get(NetworkUtil.gameEndingSpaceId);
		
		lidGenerator = new UuidGenerator();
		
		minNumberOfPlayersToStartGame = 2;
		maxNumberOfPlayersInLobby = 6;
	}
	
	public void setListener(ServerListener listener) {
		this.listener = listener;
	}
	
	public void run() {
		listenForAuthentication();
		listenForUserRequests();
		listenForDisconnect();
		listenForGameServerTermination();
	}
	
	private void listenForGameServerTermination() {
		new Thread(() -> {
			
			FixedLoopProcess process = new FixedLoopProcess() {
				
				@Override
				public void update(float dt) {
					
					try {
						List<Object[]> terminateTuples = gameEndingSpace.getAll(
								new ActualField(Actors.server),
								new FormalField(Integer.class),
								new FormalField(List.class));
						
						for (Object[] terminateTuple : terminateTuples) {
							int gid = (int) terminateTuple[1];
							List<Integer> top3Uids = (ArrayList<Integer>) terminateTuple[2];
							
							GameInfo gameInfo = gidToGameInfo.remove(gid);
							leaveGameUserInfoUpdate(gameInfo);
							incrementGamesPlayed(gameInfo);
							updateUserWins(top3Uids);
							
							// acknowledge.
							gameEndingSpace.put(gid);
						}
						
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			};
			
			process.setTps(10);
			process.startLoop();
			
		}).start();
	}
	
	private void incrementGamesPlayed(GameInfo gameInfo) {
		for (int uid : gameInfo.playerUids) {
			UserInfo userInfo = uidToUserInfo.get(uid);
			userInfo.gamesPlayed += 1;
		}
	}
	
	private void leaveGameUserInfoUpdate(GameInfo gameInfo) {
		for (int uid : gameInfo.playerUids) {
			UserInfo userInfo = uidToUserInfo.get(uid);
			userInfo.inGame = false;
		}
	}

	private void updateUserWins(List<Integer> top3Uids) {
		if (top3Uids.size() == 2) {
			int uid1 = top3Uids.get(0);
			int uid2 = top3Uids.get(1);
			
			UserInfo user1 = uidToUserInfo.get(uid1);
			UserInfo user2 = uidToUserInfo.get(uid2);

			user1.firstPlaces += 1;
			user2.secondPlaces += 1;
			
		} else if (top3Uids.size() == 3) {
			int uid1 = top3Uids.get(0);
			int uid2 = top3Uids.get(1);
			int uid3 = top3Uids.get(2);
			
			UserInfo user1 = uidToUserInfo.get(uid1);
			UserInfo user2 = uidToUserInfo.get(uid2);
			UserInfo user3 = uidToUserInfo.get(uid3);
			
			user1.firstPlaces += 1;
			user2.secondPlaces += 1;
			user3.thirdPlaces += 1;
			
		} else {
			throw new IllegalStateException("broken");
		}
	}

	private void listenForDisconnect() {
		new Thread(() -> {
			
			FixedLoopProcess process = new FixedLoopProcess() {
				
				@Override
				public void update(float dt) {
					
					try {
						List<Object[]> disconnectTuples = authSafeSpace.getAll(
								new ActualField(Actors.server),
								new ActualField(DisconnectMessage.disconnected),
								new FormalField(Integer.class));
						
						for (Object[] disconnectTuple : disconnectTuples) {
							int uid = (int) disconnectTuple[2];
							onSignOutOrDisconnect(uid);
						}
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			};
			
			process.setTps(10);
			process.startLoop();
			
		}).start();
	}

 /*
  *  "conn" is used for the protocol, because it makes us able to check disconnects.
  *  If we used "keep", then if a client disconnected an exception will be thrown in 
  *  a thread (inbox/outbox threads) created in the JSpace lib.
  *  We can not catch this exception and handle the disconnect.
  *  Thus we can not use "keep".
  *  The problem with "conn" is that the blocking threads (.get()) will not always wake up
  *  when a tuple arrives. So we need to create out own loops and check every x ms.
  *  Using "keep", the threads wake up correctly. 
  *  We can do a work-around with "conn", which gives us the desired behavior.
  */
	
	private void listenForAuthentication() {
		new Thread(() -> {
			FixedLoopProcess process = new FixedLoopProcess() {
				
				@Override
				public void update(float dt) {
					try {
						List<Object[]> requestTuples = authenticationSpace.getAll(
								new ActualField(Actors.server),
								new FormalField(AuthenticationMessage.class),
								new FormalField(String.class),	// client public key
								new FormalField(Object.class));	// identifier (either credentials or uid)
						
						for (Object[] requestTuple : requestTuples) {
							AuthenticationMessage requestMessage = (AuthenticationMessage) requestTuple[1];
							String clientPublicKey = (String) requestTuple[2];
							Object clientIdentifier = requestTuple[3];
							
							switch (requestMessage) {
								case signUp: {
									Credentials encryptedCredentials = (Credentials) clientIdentifier;
									Credentials decryptedCredentials = encryption.decrypt(encryptedCredentials, privateKey);
									
									authSafeSpace.put(Actors.identityProvider, requestMessage, decryptedCredentials);
									
									// get response from identity-provider.
									Object[] serverTuple = authSafeSpace.get(
											new ActualField(clientIdentifier),
											new ActualField(requestMessage),
											new FormalField(Object.class));
									
									Object serverPacket = serverTuple[2];
									SignUpServerPacket packet = (SignUpServerPacket) serverPacket;
									
									if (packet.status == SignUpStatus.success) {
										registerUser(packet.uid, packet.username);
	
										listener.onChange();
									}
									
									Credentials serverEncryptedCredentials = encryption.encrypt(decryptedCredentials, clientPublicKey);
									
									SignUpResponsePacket response = new SignUpResponsePacket();
									response.status = packet.status;
									authenticationSpace.put(serverEncryptedCredentials, AuthenticationMessage.signUp, response);
									
									break;
								}
								
								case signIn: {
									Credentials encryptedCredentials = (Credentials) clientIdentifier;
									Credentials decryptedCredentials = encryption.decrypt(encryptedCredentials, clientPublicKey);
									
									authSafeSpace.put(Actors.identityProvider, requestMessage, decryptedCredentials);
									
									// get response from identity-provider.
									Object[] serverTuple = authSafeSpace.get(
											new ActualField(clientIdentifier),
											new ActualField(requestMessage),
											new FormalField(Object.class));
									
									Object serverPacket = serverTuple[2];
									
									SignInServerPacket packet = (SignInServerPacket) serverPacket;
									
									int uid = packet.uid;
	
									if (packet.status == SignInStatus.success) {
										UserInfo userInfo = uidToUserInfo.get(uid);
										userInfo.online = true;
										
										listener.onChange();
									}
									
									Credentials serverEncryptedCredentials = encryption.encrypt(encryptedCredentials, privateKey);

									SignInResponsePacket response = new SignInResponsePacket();
									response.status = packet.status;
									response.uid = uid;
									authenticationSpace.put(serverEncryptedCredentials, AuthenticationMessage.signIn, response);
									
									break;
								}
								
								case signOut: {
									authSafeSpace.put(Actors.identityProvider, requestMessage, clientIdentifier);
									
									// get response from identity-provider.
									Object[] serverTuple = authSafeSpace.get(
											new ActualField(clientIdentifier),
											new ActualField(requestMessage),
											new FormalField(Object.class));
									
									Object serverPacket = serverTuple[2];
									
									int uid = (int) serverPacket;
	
									onSignOutOrDisconnect(uid);
									
									listener.onChange();
									
									break;
								}
							}
						}
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			};
			
			process.setTps(10);
			process.startLoop();
			
		}).start();
	}
	

	private void onSignOutOrDisconnect(int uid) throws InterruptedException {
		UserInfo userInfo = uidToUserInfo.get(uid);
		userInfo.online = false;
		
		if (userInfo.inLobby) {
			userInfo.inLobby = false;
			
			int lid = userInfo.lid;
			LobbyInfo lobbyInfo = lidToLobbyInfo.get(lid);
			if (lobbyInfo.ownerUid == uid) {
				for (int playerUid : lobbyInfo.playerUids) {
					UserInfo player = uidToUserInfo.get(playerUid);
					player.inLobby = false;
				}
				broadcastLobbyTermination(getLobbyMembersExceptYou(uid));
				lidToLobbyInfo.remove(userInfo.lid);
			} else {
				// user leaves lobby
				lobbyInfo.playerUids.remove((Integer) uid);
				broadcastLobbyLeave(getLobbyMembersExceptYou(uid), uid);
			}
			
			listener.onChange();
		}
	}

	private void registerUser(int uid, String username) {
		UserInfo info = new UserInfo();
		info.username = username;
		info.uid = uid;
		info.gamesPlayed = 0;
		info.firstPlaces = 0;
		info.secondPlaces = 0;
		info.thirdPlaces = 0;
		uidToUserInfo.put(uid, info);
	}
	
	private void listenForUserRequests() {
		new Thread(() -> {
			FixedLoopProcess process = new FixedLoopProcess() {
				
				@Override
				public void update(float dt) {
					try {
						 List<Object[]> requestTuples = userSpace.getAll(
								 new ActualField(Actors.server), 
								 new FormalField(UserMessages.class),
								 new FormalField(Object.class));
						 

						 for (Object[] requestTuple : requestTuples) {
							 UserMessages message = (UserMessages) requestTuple[1];
							 Object packet = requestTuple[2];
							 
							 switch (message) {
								 case userInfo: {
									 int uid = (int) packet;
									 
									 UserInfo userInfo = uidToUserInfo.get(uid);
									 if (userInfo.online) {
										 UserInfoPacket response = new UserInfoPacket();
										 response.username = userInfo.username;
										 response.uid = userInfo.uid;
										 response.inGame = userInfo.inGame;
										 response.gameSpacePath = userInfo.gameGate;
										 response.firstPlaces = userInfo.firstPlaces;
										 response.secondPlaces = userInfo.secondPlaces;
										 response.thirdPlaces = userInfo.thirdPlaces;
										 response.gamesPlayed = userInfo.gamesPlayed;
										 userSpace.put(uid, UserMessages.userInfo, response);
									 }
									 
									 break;
								 }
							 
							 case createLobby: {
								 LobbyCreationRequestPacket request = (LobbyCreationRequestPacket) packet;
								 
								 UserInfo userInfo = uidToUserInfo.get(request.uid);
								 if (userInfo.online) {
									 LobbyCreationResponsePacket response = new LobbyCreationResponsePacket();
									
									 if (!userInfo.inLobby) {
										 
										 if (validLobbyName(request.name)) {
											 int lid = lidGenerator.getUuid();
						
											 registerLobby(request.uid, request.name, lid);
											 
											 LobbyInfo lobbyInfo = lidToLobbyInfo.get(lid);
											 response.lobbyInfo = getClientLobbyInfo(lobbyInfo);
											 response.created = true;
	
											 listener.onChange();
	
										 } else {
											 response.created = false;
										 }
									 } else {
										 response.created = false;
									 }
									 
									 userSpace.put(request.uid, UserMessages.createLobby, response);
								 }
								 
								 break; 
							 }
							 
							 case startGame: {
								 int uid = (int) packet;
								 
								 UserInfo userInfo = uidToUserInfo.get(uid);
								 if (userInfo.online) {
									 if (userInfo.inLobby) {
										 LobbyInfo lobbyInfo = lidToLobbyInfo.get(userInfo.lid);
										 if (lobbyInfo.ownerUid == uid) {
											 StartGameResponse response = new StartGameResponse();
											 
											 int numberOfPlayers = lobbyInfo.playerUids.size();
											 if (numberOfPlayers >= lobbyInfo.minNumberOfPlayersToStartGame) {
												 response.success = true;
												 
												 GameInfo gameInfo = promoteLobbyToGame(lobbyInfo);
												 
												 for (int uids : lobbyInfo.playerUids) {
														UserInfo users = uidToUserInfo.get(uids);
														users.inGame = true;
														users.inLobby = false;
														users.gameGate = gameInfo.gameSpaceId;
												 }
												 
												 spawnGameServer(gameInfo);
	
												 // broadcastGameStart(getLobbyMembersExceptYou(uid), lobbyInfo.lid);
												 broadcastGameStart(getLobbyMembers(uid), gameInfo.gameSpaceId);
	
												 lidToLobbyInfo.remove(lobbyInfo.lid);
												 
												 listener.onChange();
												 
											 } else {
												 response.success = false;
											 }
	
											 userSpace.put(uid, UserMessages.startGame, response);
										 }
									 }
								 }
								 break;
							 }
							 
							 case terminateLobby: {
								 int uid = (int) packet;
								 
								 UserInfo userInfo = uidToUserInfo.get(uid);
								 if (userInfo.online) {
									 if (userInfo.inLobby) {
										 LobbyInfo lobbyInfo = lidToLobbyInfo.get(userInfo.lid);
										 if (lobbyInfo.ownerUid == uid) {
											 
											 for (int playerUid : lobbyInfo.playerUids) {
												 UserInfo player = uidToUserInfo.get(playerUid);
												 player.inLobby = false;
											 }
											 
											 broadcastLobbyTermination(getLobbyMembers(uid));
											 lidToLobbyInfo.remove(userInfo.lid);
											 
											 listener.onChange();
										 }
									 }
								 }
								 break;
							 }
							 
							 case lobbyList: {
								 int uid = (int) packet;
								 
								 UserInfo userInfo = uidToUserInfo.get(uid);
								 if (userInfo.online) {
									 if (!userInfo.inLobby) {
										 LobbyListResponsePacket response = new LobbyListResponsePacket();
										 
										 for (LobbyInfo lobbyInfo : lidToLobbyInfo.values()) {
											 ClientLobbyInfo clientLobbyInfo = getClientLobbyInfo(lobbyInfo);
											 response.lobbies.add(clientLobbyInfo);
										 }
										 
										 userSpace.put(uid, UserMessages.lobbyList, response);
										 
										 listener.onChange();
									 }
								 }
								 
								 break;
							 }
							 
							 case joinLobby: {
								 JoinLobbyRequest request = (JoinLobbyRequest) packet;
	
								 int uid = request.uid;
								 int lid = request.lid;
								 
								 UserInfo userInfo = uidToUserInfo.get(uid);
								 if (userInfo.online) {
									 if (!userInfo.inLobby) {
										 LobbyInfo lobbyInfo = lidToLobbyInfo.get(lid);
										 
										 JoinLobbyResponse response = new JoinLobbyResponse();
	
										 if (lobbyInfo != null) {
											 int numberOfPlayers = lobbyInfo.playerUids.size();
											 if (numberOfPlayers < lobbyInfo.maxNumberOfPlayers) {
												 userInfo.inLobby = true;
												 userInfo.lid = lid;
												 
												 lobbyInfo.playerUids.add(uid);
												 
												 response.info = getClientLobbyInfo(lobbyInfo);
												 response.success = true;
												 
												 userSpace.put(uid, UserMessages.joinLobby, response);
												 
												 broadcastLobbyJoin(getLobbyMembersExceptYou(uid), uid);
												 
												 listener.onChange();
											 } else {
												 response.success = false;
												 userSpace.put(uid, UserMessages.joinLobby, response);
											 }
										 } else {
											 response.success = false;
											 userSpace.put(uid, UserMessages.joinLobby, response);
										 }
									 }
								 }
								 
								 break;
							 }
							 
							 case leaveLobby: {
								 int uid = (int) packet;
								 
								 UserInfo userInfo = uidToUserInfo.get(uid);
								 if (userInfo.online) {
									 if (userInfo.inLobby) {
										 userInfo.inLobby = false;
										 
										 LobbyInfo lobbyInfo = lidToLobbyInfo.get(userInfo.lid);
										 lobbyInfo.playerUids.remove((Integer) uid);
										 
										 broadcastLobbyLeave(getLobbyMembers(uid), uid);
										 
										 listener.onChange();
									 }
								 }
								 
								 break;
							 }
	
							 default:
								 throw new NonExhaustiveSwitchException();
							 }
						 }

						} catch (InterruptedException e) {
							e.printStackTrace();
						}
				}
			};
			
			process.setTps(10);
			process.startLoop();
		
		}).start();
	}

	private void spawnGameServer(GameInfo gameInfo) {
		String gate = NetworkUtil.getGate(ip4, gameInfo.gameSpaceId);
		
		repository.add(gameInfo.gameSpaceId, new SequentialSpace());
		repository.addGate(gate);
		
		new Thread(() -> {
			 GameServer gameServer = new GameServer(repository, gameInfo);
			 gameServer.startLoop();
		 }).start();
	}
	
	private GameInfo promoteLobbyToGame(LobbyInfo lobbyInfo) {
		GameInfo gameInfo = new GameInfo();
		gameInfo.gid = lobbyInfo.lid;
		gameInfo.name = lobbyInfo.name;
		gameInfo.playerUids =  lobbyInfo.playerUids;
		gameInfo.phase = GamePhase.starting;
		gameInfo.gameSpaceId = getGameServerName(gameInfo.gid);
		gidToGameInfo.put(gameInfo.gid, gameInfo);
		return gameInfo;
	}
	
	private String getGameServerName(int lid) {
		return String.format("%s/%d", NetworkUtil.gameSpaceId, lid);
	}

	private boolean validLobbyName(String name) {
		return name.length() > 0;
	}

	private void registerLobby(int uid, String name, int lid) {
		LobbyInfo lobbyInfo = new LobbyInfo();
		lobbyInfo.lid = lid;
		lobbyInfo.name = name;
		lobbyInfo.ownerUid = uid;
		lobbyInfo.minNumberOfPlayersToStartGame = minNumberOfPlayersToStartGame;
		lobbyInfo.maxNumberOfPlayers = maxNumberOfPlayersInLobby;
		lobbyInfo.playerUids.add(uid);
		lidToLobbyInfo.put(lid, lobbyInfo);
		
		UserInfo owner = uidToUserInfo.get(uid);
		owner.inLobby = true;
		owner.lid = lid;
	}

	private void broadcastLobbyLeave(List<Integer> receiverUids, int leavingUid) throws InterruptedException {
		UserInfo info = uidToUserInfo.get(leavingUid);
		for (int uid : receiverUids) {
			userSpace.put(uid, BroadcastMessages.leave, info.username);
		}
	}
	
	private void broadcastLobbyJoin(List<Integer> receiverUids, int joiningUid) throws InterruptedException {
		UserInfo info = uidToUserInfo.get(joiningUid);
		for (int uid : receiverUids) {
			userSpace.put(uid, BroadcastMessages.join, info.username);
		}
	}
	
	private List<Integer> getLobbyMembers(int uid) {
		UserInfo userInfo = uidToUserInfo.get(uid);
		LobbyInfo lobbyInfo = lidToLobbyInfo.get(userInfo.lid);
		return lobbyInfo.playerUids;
	}

	private List<Integer> getLobbyMembersExceptYou(int uid) {
		UserInfo userInfo = uidToUserInfo.get(uid);
		LobbyInfo lobbyInfo = lidToLobbyInfo.get(userInfo.lid);
		
		List<Integer> players = new ArrayList<>();
		players.addAll(lobbyInfo.playerUids);
		players.remove((Integer) uid);
		
		return players;
	}
	
	private ClientLobbyInfo getClientLobbyInfo(LobbyInfo lobbyInfo) {
		ClientLobbyInfo clientInfo = new ClientLobbyInfo();
		clientInfo.name = lobbyInfo.name;
		clientInfo.lid = lobbyInfo.lid;

		UserInfo ownerInfo = uidToUserInfo.get(lobbyInfo.ownerUid);
		clientInfo.owner = ownerInfo.username;
		
		clientInfo.minNumberOfPlayersToStartGame = lobbyInfo.minNumberOfPlayersToStartGame;
		clientInfo.maxNumberOfPlayers = lobbyInfo.maxNumberOfPlayers;

		for (int playerUid : lobbyInfo.playerUids) {
			 UserInfo playerInfo = uidToUserInfo.get(playerUid);
			 clientInfo.players.add(playerInfo.username);
		}
		
		return clientInfo;
	}
	
	private void broadcastLobbyTermination(List<Integer> receiverUids) throws InterruptedException {
		
		for (int uid : receiverUids) {
			userSpace.put(uid, BroadcastMessages.terminate, -1);
		}
	}
	
	private void broadcastGameStart(List<Integer> receiverUids, String path) throws InterruptedException {
		for (int uid : receiverUids) {
			userSpace.put(uid, BroadcastMessages.start, path);
		}
	}
	
	public Collection<UserInfo> getUserInfos() {
		return uidToUserInfo.values();
	}

	public Collection<LobbyInfo> getLobbyInfos() {
		return lidToLobbyInfo.values();
	}
}
