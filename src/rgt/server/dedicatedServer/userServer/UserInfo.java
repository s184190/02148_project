package rgt.server.dedicatedServer.userServer;

public class UserInfo {
	public int uid;
	public String username;
	
	public boolean online;
	
	public int lid;
	public boolean inLobby;
	public boolean inGame;
	public String gameGate;

	public int firstPlaces;
	public int secondPlaces;
	public int thirdPlaces;
	public int gamesPlayed;
}
