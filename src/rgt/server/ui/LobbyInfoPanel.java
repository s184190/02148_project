package rgt.server.ui;

import javax.swing.JLabel;

import rgt.server.dedicatedServer.userServer.LobbyInfo;
import rgt.ui.common.RowPanel;

public class LobbyInfoPanel extends RowPanel {

	private JLabel IDField;
	private JLabel nameField;
	private JLabel ownerField;
	private JLabel membersField;

	public LobbyInfoPanel() {
		this.IDField = new JLabel();
		this.nameField = new JLabel();
		this.ownerField = new JLabel();
		this.membersField = new JLabel();

		addFieldRow(new JLabel("ID:"), IDField);
		addFieldRow(new JLabel("Name:"), nameField);
		addFieldRow(new JLabel("Owner:"), ownerField);
		addFieldRow(new JLabel("Members:"), membersField);
	}

	public void setLobbyInfo(LobbyInfo info) {
		IDField.setText(info.lid + "");
		nameField.setText(info.name);
		ownerField.setText(info.ownerUid + "");
		membersField.setText(info.playerUids.size() + "");
	}
}
