package rgt.server.ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.List;

import javax.swing.BorderFactory;

import rgt.server.dedicatedServer.userServer.UserInfo;
import rgt.ui.common.Tab;

public class UserTab extends Tab {

	private final UserListPanel userListPanel;
	private final UserInfoPanel userInfoPanel;

	private List<UserInfo> accountsInfos;

	public UserTab(List<UserInfo> accoutInfos) {
		this.accountsInfos = accoutInfos;

		setOpaque(false);

		userListPanel = new UserListPanel();
		userListPanel.setBorder(BorderFactory.createTitledBorder("Users in server"));
		userInfoPanel = new UserInfoPanel();
		userInfoPanel.setBorder(BorderFactory.createTitledBorder("User Information"));

		uiLayout();
		uiEvents();
	}

	private void uiLayout() {
		setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(5, 5, 5, 5);
		constraints.fill = GridBagConstraints.NONE;
		constraints.anchor = GridBagConstraints.NORTH;
		constraints.weighty = 1.0;

		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(userListPanel, constraints);
		constraints.fill = GridBagConstraints.NONE;

		constraints.gridx = 1;
		constraints.gridy = 0;
		constraints.anchor = GridBagConstraints.NORTH;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(userInfoPanel, constraints);
		userInfoPanel.setOpaque(false);
	}

	private void uiEvents() {
		userListPanel.addSelectionListener((e) -> {
			updateUserSelection();
		});
	}

	private void updateUserSelection() {
		UserInfo info = userListPanel.getSelectedAccountInfo();
		if (info != null) {
			userInfoPanel.setUserInfo(info);
		}
	}

	@Override
	public void enter() {
		refreshTable();
	}

	public void refreshTable() {
		userListPanel.updateCells(accountsInfos);
		updateUserSelection();
	}

	@Override
	public void leave() {
	}
}
