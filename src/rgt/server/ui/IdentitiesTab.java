package rgt.server.ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.List;

import javax.swing.BorderFactory;

import rgt.server.dedicatedServer.identityProvider.IdentityInfo;
import rgt.ui.common.Tab;

public class IdentitiesTab extends Tab {

	private final IdentityListPanel idListPanel;
	private final IdentityInfoPanel idInfoPanel;
	
	private List<IdentityInfo> identityInfos;
	
	public IdentitiesTab(List<IdentityInfo> accoutInfos) {
		this.identityInfos = accoutInfos;
		
		setOpaque(false);
	
		idListPanel = new IdentityListPanel();
		idListPanel.setBorder(BorderFactory.createTitledBorder("ID's"));
		idInfoPanel = new IdentityInfoPanel();
		idInfoPanel.setBorder(BorderFactory.createTitledBorder("ID info"));
		
		uiLayout();
		uiEvents();
	}
	
	private void uiLayout() {
		setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		
		setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(5, 5, 5, 5);
		constraints.fill = GridBagConstraints.NONE;
		constraints.anchor = GridBagConstraints.NORTH;
		constraints.weighty = 1.0;
			
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(idListPanel, constraints);
		constraints.fill = GridBagConstraints.NONE;
		
		constraints.gridx = 1;
		constraints.gridy = 0;
		constraints.anchor = GridBagConstraints.NORTH;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(idInfoPanel, constraints);
		idInfoPanel.setOpaque(false);
	}
	
	private void uiEvents() {
		
		idListPanel.addSelectionListener((e) -> {
			updateIdentitySelection();
		});
	}

	private void updateIdentitySelection() {
		IdentityInfo info = idListPanel.getSelectedIdentityInfo();
		if (info != null) {
			idInfoPanel.setIdentityInfo(info);
		}
	}
	
	@Override
	public void enter() {
		idListPanel.updateCells(identityInfos);
		updateIdentitySelection();
	}
	
	public void refreshTable() {
		idListPanel.updateCells(identityInfos);
		updateIdentitySelection();
	}

	@Override
	public void leave() {
	}
}
