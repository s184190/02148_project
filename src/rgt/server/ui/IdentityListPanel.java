	package rgt.server.ui;

import java.util.List;

import javax.swing.table.DefaultTableModel;

import rgt.server.dedicatedServer.identityProvider.IdentityInfo;
import rgt.ui.common.TableListPanel;

public class IdentityListPanel extends TableListPanel {

	private static final int usernameColumn = 0;
	private static final int passwordColumn = 1;
	private static final int IDColumn = 2;
	private static final int aliveColumn = 3;
	
	private static final String[] USER_COLUMN = new String[] {
		"Username",
		"Password",
		"ID",
		"Online"
	};
	
	public IdentityListPanel() {
		super(USER_COLUMN);
	}
	
	public void updateCells(List<IdentityInfo> infos) {
		DefaultTableModel tableModel = getModel();
		
		int row = 0;
		tableModel.setRowCount(infos.size());
		for (IdentityInfo info : infos) {
			tableModel.setValueAt(info.username, row, usernameColumn);
			tableModel.setValueAt(info.password, row, passwordColumn);
			tableModel.setValueAt(info.uid, row, IDColumn);
			tableModel.setValueAt(info.alive, row, aliveColumn);
			row += 1;
		}
		
		if (tableModel.getRowCount() != 0)
			getTable().getSelectionModel().setSelectionInterval(0, 0);
	}
	
	public IdentityInfo getSelectedIdentityInfo() {
		DefaultTableModel tableModel = getModel();
		if (tableModel.getRowCount() <= 0)
			return null;
		
		int index = getTable().getSelectedRow();
		String username = (String)tableModel.getValueAt(index, usernameColumn);
		String password = (String)tableModel.getValueAt(index, passwordColumn);
		int ID = (int)tableModel.getValueAt(index, IDColumn);
		boolean alive = (boolean) tableModel.getValueAt(index, aliveColumn);
		
		IdentityInfo info = new IdentityInfo();
		info.username = username;
		info.password = password;
		info.uid = ID;
		info.alive = alive;
		
		return info;
	}
}
