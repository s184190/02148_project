package rgt.server.ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.List;

import javax.swing.BorderFactory;

import rgt.server.dedicatedServer.userServer.LobbyInfo;
import rgt.ui.common.Tab;

public class LobbyTab extends Tab {

	private final LobbyListPanel lobbyListPanel;
	private final LobbyInfoPanel lobbyInfoPanel;
	
	private List<LobbyInfo> lobbyInfo;
	
	public LobbyTab(List<LobbyInfo> lobbyInfo) {
		this.lobbyInfo = lobbyInfo;
		
		setOpaque(false);
	
		lobbyListPanel = new LobbyListPanel();
		lobbyListPanel.setBorder(BorderFactory.createTitledBorder("Lobbies"));
		
		lobbyInfoPanel = new LobbyInfoPanel();
		lobbyInfoPanel.setBorder(BorderFactory.createTitledBorder("Lobby info"));
		
		uiLayout();
		uiEvents();
	}
	
	private void uiLayout() {
		setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		
		setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(5, 5, 5, 5);
		constraints.fill = GridBagConstraints.NONE;
		constraints.anchor = GridBagConstraints.NORTH;
		constraints.weighty = 1.0;
			
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(lobbyListPanel, constraints);
		constraints.fill = GridBagConstraints.NONE;
		
		constraints.gridx = 1;
		constraints.gridy = 0;
		constraints.anchor = GridBagConstraints.NORTH;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(lobbyInfoPanel, constraints);
		lobbyInfoPanel.setOpaque(false);
	}

	private void uiEvents() {
		lobbyListPanel.addSelectionListener((e) -> {
			updateLobbySelection();
		});
	}
	
	
	private void updateLobbySelection() {
		LobbyInfo info = lobbyListPanel.getSelectedLobbyInfo();
		if (info != null) {
			lobbyInfoPanel.setLobbyInfo(info);
		}
	}
	
	@Override
	public void enter() {
		lobbyListPanel.updateCells(lobbyInfo);
		updateLobbySelection();
	}
	
	public void refreshTable() {
		lobbyListPanel.updateCells(lobbyInfo);
		updateLobbySelection();
	}
	

	@Override
	public void leave() {
	}
}
