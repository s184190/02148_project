package rgt.server.ui;

import javax.swing.JLabel;

import rgt.server.dedicatedServer.userServer.UserInfo;
import rgt.ui.common.RowPanel;

public class UserInfoPanel extends RowPanel {

	private JLabel idField;
	private JLabel usernameField;
	private JLabel isJoinedField;
	private JLabel joinedIDField;

	public UserInfoPanel() {
		this.idField = new JLabel();
		this.usernameField = new JLabel();
		this.isJoinedField = new JLabel();
		this.joinedIDField = new JLabel();

		addFieldRow(new JLabel("ID:"), idField);
		addFieldRow(new JLabel("Username:"), usernameField);
		addFieldRow(new JLabel("In a Lobby:"), isJoinedField);
		addFieldRow(new JLabel("Joined Lobby ID:"), joinedIDField);
	}

	public void setUserInfo(UserInfo info) {
		idField.setText((info.uid + ""));
		usernameField.setText(info.username);
		isJoinedField.setText(info.inLobby + "");
		joinedIDField.setText(info.lid + "");
	}
}
