	package rgt.server.ui;

import java.util.List;

import javax.swing.table.DefaultTableModel;

import rgt.server.dedicatedServer.userServer.LobbyInfo;
import rgt.ui.common.TableListPanel;

public class LobbyListPanel extends TableListPanel {

	private static final int IDColumn = 0;
	private static final int nameColumn = 1;
	private static final int ownerColumn = 2;
	private static final int nrOfMembersColumn = 3;
	
	private List<Integer> playerUids;
	private List<LobbyInfo> lobbyInfos;
	
	private static final String[] LOBBY_COLUMN = new String[] {
		"ID's",
		"Names",
		"Owner",
		"Members",
	};
	
	public LobbyListPanel() {
		super(LOBBY_COLUMN);
	}
	
	public void updateCells(List<LobbyInfo> infos) {
		DefaultTableModel tableModel = getModel();
		
		int row = 0;
		tableModel.setRowCount(infos.size());
		for (LobbyInfo info : infos) {
			tableModel.setValueAt(info.lid, row, IDColumn);
			tableModel.setValueAt(info.name, row, nameColumn);
			tableModel.setValueAt(info.ownerUid, row, ownerColumn);
			tableModel.setValueAt(info.playerUids.size(), row, nrOfMembersColumn);
			playerUids = info.playerUids;
			row += 1;
		}
		
		if (tableModel.getRowCount() != 0)
			getTable().getSelectionModel().setSelectionInterval(0, 0);
	}
	
	public LobbyInfo getSelectedLobbyInfo() {
		DefaultTableModel tableModel = getModel();
		if (tableModel.getRowCount() <= 0)
			return null;
		
		int index = getTable().getSelectedRow();
		if (index == -1) return null;
		
		int ID = (int)tableModel.getValueAt(index, IDColumn);
		String name = (String)tableModel.getValueAt(index, nameColumn);
		int owner = (int)tableModel.getValueAt(index, ownerColumn);
		int members = (int)tableModel.getValueAt(index, nrOfMembersColumn);
		
		LobbyInfo info = new LobbyInfo();
		info.lid = ID;
		info.name = name;
		info.ownerUid = owner;
		info.playerUids = playerUids;
		
		return info;
	}
}
