package rgt.server.ui;

import javax.swing.JLabel;

import rgt.server.dedicatedServer.identityProvider.IdentityInfo;
import rgt.ui.common.RowPanel;

public class IdentityInfoPanel extends RowPanel {

	private JLabel usernameField;
	private JLabel passwordField;
	private JLabel uidField;
	private JLabel aliveField;

	public IdentityInfoPanel() {
		this.usernameField = new JLabel();
		this.passwordField = new JLabel();
		this.uidField = new JLabel();
		this.aliveField = new JLabel();

		addFieldRow(new JLabel("Username:"), usernameField);
		addFieldRow(new JLabel("Password:"), passwordField);
		addFieldRow(new JLabel("ID:"), uidField);
		addFieldRow(new JLabel("Online:"), aliveField);
	}

	public void setIdentityInfo(IdentityInfo info) {
		usernameField.setText(info.username);
		passwordField.setText(info.password);
		uidField.setText(info.uid + "");
		aliveField.setText(info.alive + "");
	}
}
