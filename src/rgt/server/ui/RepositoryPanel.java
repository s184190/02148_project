package rgt.server.ui;

import java.util.List;

import rgt.server.dedicatedServer.identityProvider.IdentityInfo;
import rgt.server.dedicatedServer.userServer.LobbyInfo;
import rgt.server.dedicatedServer.userServer.UserInfo;
import rgt.ui.common.TabPanel;

public class RepositoryPanel extends TabPanel {

	private UserTab usersTab;
	private IdentitiesTab identityTab;
	private LobbyTab lobbyTab;
	
	public RepositoryPanel(List<UserInfo> userInfos, List<IdentityInfo> identityInfo, List<LobbyInfo> lobbyInfo) {
		super("Repositories");
		
		usersTab = new UserTab(userInfos);
		identityTab = new IdentitiesTab(identityInfo);
		lobbyTab = new LobbyTab(lobbyInfo);
		
		setupTabs();
	}

	private void setupTabs() {
		addTab("Users", usersTab);
		addTab("Identities", identityTab);
		addTab("Lobbys", lobbyTab);
		
		setInitialTab(usersTab);
	}

	public void refreshUserTab() {
		usersTab.refreshTable();
	}
	
	public void refreshIdentityTab() {
		identityTab.refreshTable();
	}
}
