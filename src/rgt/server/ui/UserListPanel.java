package rgt.server.ui;

import java.util.List;

import javax.swing.table.DefaultTableModel;

import rgt.server.dedicatedServer.userServer.UserInfo;
import rgt.ui.common.TableListPanel;

public class UserListPanel extends TableListPanel {

	private static final int IDColumn = 0;
	private static final int usernameColumn = 1;
	private static final int isJoinedColumn = 2;
	private static final int joinedIDColumn = 3;
	
	private static final String[] USER_COLUMN = new String[] {
		"ID",
		"Username",
		"In a Lobby",
		"Joined Lobby ID"
	};
	
	public UserListPanel() {
		super(USER_COLUMN);
	}
	
	public void updateCells(List<UserInfo> accounts) {
		DefaultTableModel tableModel = getModel();
		
		int row = 0;
		tableModel.setRowCount(accounts.size());
		for (UserInfo account : accounts) {
			tableModel.setValueAt(account.uid, row, IDColumn);
			tableModel.setValueAt(account.username, row, usernameColumn);
			tableModel.setValueAt(account.inLobby, row, isJoinedColumn);
			tableModel.setValueAt(account.lid, row, joinedIDColumn);
			row += 1;
		}
		
		if (tableModel.getRowCount() != 0)
			getTable().getSelectionModel().setSelectionInterval(0, 0);
	}
	
	public UserInfo getSelectedAccountInfo() {
		DefaultTableModel tableModel = getModel();
		if (tableModel.getRowCount() <= 0)
			return null;
		
		int index = getTable().getSelectedRow();
		int ID = (int)tableModel.getValueAt(index, IDColumn);
		String username = (String)tableModel.getValueAt(index, usernameColumn);
		boolean joinedLobby = (boolean)tableModel.getValueAt(index, isJoinedColumn);
		int joinedLobbyId = (int)tableModel.getValueAt(index, joinedIDColumn);
		
		UserInfo info = new UserInfo();
		info.username = username;
		info.inLobby = joinedLobby;
		info.lid = joinedLobbyId;
		info.uid = ID;
		
		return info;
	}
}
