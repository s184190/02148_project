package rgt.server;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

import org.jspace.SequentialSpace;
import org.jspace.SpaceRepository;

import engine.Window;
import rgt.server.dedicatedServer.identityProvider.IdentityInfo;
import rgt.server.dedicatedServer.identityProvider.IdentityProvider;
import rgt.server.dedicatedServer.identityProvider.IdentityProvidierListener;
import rgt.server.dedicatedServer.userServer.LobbyInfo;
import rgt.server.dedicatedServer.userServer.ServerListener;
import rgt.server.dedicatedServer.userServer.TetrisServer;
import rgt.server.dedicatedServer.userServer.UserInfo;
import rgt.server.ui.RepositoryPanel;
import rgt.shared.NetworkUtil;

public class ServerLauncher {

	private SpaceRepository repository;

	private List<UserInfo> userInfos;
	private List<IdentityInfo> identityInfos;
	private List<LobbyInfo> lobbyInfos;
	
	private IdentityProvider identityProvider;
	private TetrisServer server;
	
	private RepositoryPanel panel;
	
	public ServerLauncher() {
		userInfos = new ArrayList<>();
		identityInfos = new ArrayList<>();
		lobbyInfos = new ArrayList<>();
		
		Window window = new Window();
		WindowAdapter closeAdapter = new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				repository.closeGates();
				window.close();
			}
		};

		panel = new RepositoryPanel(userInfos, identityInfos, lobbyInfos);
		
		String title = "tetris server";
		int width = 500;
		int height = 500;
		boolean resizable = true;
		
		window.init(title, width, height, resizable, closeAdapter);
		window.setRoot(panel);
		
		panel.enter();

		repository = new SpaceRepository();
		repository.add(NetworkUtil.authenticationSpaceId, new SequentialSpace());
		repository.add(NetworkUtil.tetrisSpaceId, new SequentialSpace());
		repository.add(NetworkUtil.aliveSpaceId, new SequentialSpace());
		repository.add(NetworkUtil.authSafeId, new SequentialSpace());
		repository.add(NetworkUtil.gameEndingSpaceId, new SequentialSpace());
		
		
		String ip4 = NetworkUtil.getIP4Address();
		
		panel.setSideTitle("ip4:", ip4);
		
		String authenticationGate = NetworkUtil.getGate(ip4, NetworkUtil.authenticationSpaceId);
		repository.addGate(authenticationGate);

		String userGate = NetworkUtil.getGate(ip4, NetworkUtil.tetrisSpaceId);
		repository.addGate(userGate);
		
		String aliveGate = NetworkUtil.getGate(ip4, NetworkUtil.aliveSpaceId);
		repository.addGate(aliveGate);
		
		identityProvider = new IdentityProvider(repository);
		server = new TetrisServer(repository, ip4);
		
		server.setListener(new ServerListener() {
			@Override
			public void onChange() {
				userInfos.clear();
				userInfos.addAll(server.getUserInfos());
				
				lobbyInfos.clear();
				lobbyInfos.addAll(server.getLobbyInfos());
				
				panel.refreshUserTab();
			}
		});
		
		identityProvider.setListener(new IdentityProvidierListener() {
			@Override
			public void onChange() {
				identityInfos.clear();
				identityInfos.addAll(identityProvider.getIdentityInfos());
				
				panel.refreshIdentityTab();
			}
		});

		server.run();
		identityProvider.run();
	}
}
