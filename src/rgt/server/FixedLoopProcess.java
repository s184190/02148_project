package rgt.server;

public abstract class FixedLoopProcess {
	
	private float msPerTick;
	private boolean running;
	
	public void setTps(int tps) {
		msPerTick = 1000 / (float) tps;
	}
	
	public abstract void update(float dt);
	
	public void startLoop() {
		running = true;
		
		boolean debug = false;
		
		float delta = 0;
		long previousTime = System.currentTimeMillis();

		while (running) {
			long now = System.currentTimeMillis();
			long elapsed = now - previousTime;
			previousTime = now;

			if (debug) {
				if (elapsed >= 2 * msPerTick)
					System.out.printf("fixedLoopProcess running behind %d\n", elapsed);
			}

			delta = elapsed / 1_000f;

			update(delta);
			sync(elapsed);
		}
	}
	
	public void stop() {
		running = false;
	}
	
	private double accumulatedDelta;
	private long theta = 3;

	private void sync(double elapsed) {
		accumulatedDelta += (elapsed - msPerTick);

		long correctedMsPerFrame = (long) (msPerTick - accumulatedDelta - theta);
		long msSleep = (correctedMsPerFrame < 0) ? 0 : correctedMsPerFrame; // @todo: yield when 0 ?
		sleep(msSleep);
	}

	private void sleep(long ms) {
		try {
			Thread.sleep(ms);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
