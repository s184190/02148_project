package rgt.ui.common;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

public class TabPanel extends JPanel {
	
	private final JTabbedPane tabbedContent;
	private Tab activeTab;
	private JLabel sideTitleLabelBold;
	private JLabel sideTitleLabelNormal;
	
	private int selectedTabIndex;
	
	private Map<Tab, Integer> tabToIndex;
	
	private boolean ready;
	
	private int numberOfTabs;
	
	public TabPanel(String title) {
		
		tabbedContent = new JTabbedPane();
		
		sideTitleLabelBold = new JLabel();
		sideTitleLabelBold.setFont(sideTitleLabelBold.getFont().deriveFont(Font.BOLD));

		sideTitleLabelNormal = new JLabel();
		
		numberOfTabs = 0;
		tabToIndex = new HashMap<>();
		
		ready = false;

		setupUiLayout(title);
		setupUiEvents();
	}
	
	private void setupUiLayout(String title) {
		setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(5, 5, 5, 5);
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.fill = GridBagConstraints.NONE;

		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.anchor = GridBagConstraints.WEST;
		add(new TitleLabel(title), constraints);

		// Add filler
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(Box.createHorizontalGlue(), constraints);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;
		
		constraints.gridx += 1;
		constraints.anchor = GridBagConstraints.EAST;
		add(sideTitleLabelNormal, constraints);
		
		constraints.gridx++;
		constraints.anchor = GridBagConstraints.EAST;
		add(sideTitleLabelBold, constraints);

		constraints.gridx = 0;
		constraints.gridy++;
		constraints.anchor = GridBagConstraints.SOUTH;
		constraints.gridwidth = 3;
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		constraints.fill = GridBagConstraints.BOTH;
		add(tabbedContent, constraints);
		constraints.fill = GridBagConstraints.NONE;
		constraints.weighty = 0.0;
		constraints.weightx = 0.0;
		constraints.gridwidth = 1;
	}
	
	protected void setInitialTab(Tab newTab) {
		activeTab = newTab;
		
		int index = getIndex(activeTab);
		if (index != -1)
			tabbedContent.setSelectedIndex(index);
	}
	
	protected void changeTab(Tab newTab) {
		if (activeTab != null) activeTab.leave();
		
		newTab.enter();
		activeTab = newTab;
		
		int index = getIndex(activeTab);
		if (index != -1)
			tabbedContent.setSelectedIndex(index);
	}

	protected void addTab(String title, Tab tabPanel) {
		tabToIndex.put(tabPanel, numberOfTabs);
		numberOfTabs += 1;
		tabbedContent.addTab(title, tabPanel);
	}

	private void setupUiEvents() {
		tabbedContent.addChangeListener((e) -> {
			if (ready && selectedTabIndex >= 0 && selectedTabIndex < tabbedContent.getTabCount()) {
				selectedTabIndex = tabbedContent.getSelectedIndex();
				Tab newTab = (Tab) tabbedContent.getComponentAt(selectedTabIndex);
				changeTab(newTab);
			}
		});
	}

	private int getIndex(Tab tabPanel) {
		int i = tabbedContent.getTabCount() - 1;
		while (i >= 0) {
			if (tabbedContent.getComponentAt(i).equals(tabPanel))
				return i;
			i--;
		}
		
		return -1;
	}
	
	public void setSideTitle(String sideTitleNormal, String sideTitleBold) {
		sideTitleLabelNormal.setText(sideTitleNormal);
		sideTitleLabelBold.setText(sideTitleBold);
	}
	
	public void enter() {
		ready = true;
		activeTab.enter();
	}
	
	public void draw() {
		activeTab.draw();
	}
	
	public void update() {
		activeTab.update();
	}
	
	public void leave() {
		activeTab.leave();
	}
	
	public void lockTab(Tab tab) {
		int tabIndex = tabToIndex.get(tab);
		tabbedContent.setEnabledAt(tabIndex, false);
	}
	
	public void unlockTab(Tab tab) {
		int tabIndex = tabToIndex.get(tab);
		tabbedContent.setEnabledAt(tabIndex, true);
	}
}
