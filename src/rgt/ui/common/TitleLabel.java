package rgt.ui.common;

import javax.swing.JLabel;

public class TitleLabel extends JLabel {

	private float fontSize = 20.0f;
	
	public TitleLabel(String msg) {
		super(msg);
		
		setFont(getFont().deriveFont(fontSize));
	}
}
