package rgt.ui.common;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

public abstract class TableListPanel extends JPanel {

	private static final int MIN_ROW_COUNT = 25;
	
	private final DefaultTableModel tableModel;
	private final JTable table;
	
	private final JScrollPane scrollPane;
	
	public TableListPanel(String[] columns) {
		setOpaque(false);
		
		tableModel = new UneditableTableModel();
		tableModel.setColumnIdentifiers(columns);
		table = new JTable(tableModel);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		table.getTableHeader().setReorderingAllowed(false);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setRowSorter(null);
		
		scrollPane = new JScrollPane(table);
		
		uiLayout();
	}
	
	private void uiLayout() {
		setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(5, 5, 5, 5);
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		constraints.fill = GridBagConstraints.BOTH;
		add(scrollPane, constraints);
		
		updateMinimumSize();
	}
	
	private void updateMinimumSize() {
		Dimension minimumSize = new Dimension(table.getPreferredSize());
		minimumSize.height = table.getRowHeight() * MIN_ROW_COUNT;
		table.setPreferredScrollableViewportSize(minimumSize);
		scrollPane.setMinimumSize(minimumSize);
	}
	
	public void clearCells() {
		tableModel.setRowCount(0);
	}
	
	@Override
    public void setEnabled(boolean enabled) {
    	super.setEnabled(enabled);
    	
    	scrollPane.setEnabled(enabled);
    	table.setEnabled(enabled);
    	table.getTableHeader().setEnabled(enabled);
	}
	
	public void setRowSelectionAllowed(boolean allowed) {
		table.setRowSelectionAllowed(allowed);
	}
	
	public void addSelectionListener(ListSelectionListener listener) {
		table.getSelectionModel().addListSelectionListener(listener);
	}
	
	public JTable getTable() {
		return table;
	}

	public DefaultTableModel getModel() {
		return tableModel;
	}
}
