package rgt.ui.common;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JTextField;

public class HintetTextField extends JTextField implements FocusListener {
	
	private String hintText;

	public HintetTextField(String hintText) {
		this.hintText = hintText;

		addFocusListener(this);
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		if (hintText != null && !hasFocus() && getText().isEmpty() && isEditable()) {
			Graphics2D g2 = (Graphics2D) g.create();
			g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

			Font italic = g2.getFont().deriveFont(Font.ITALIC);
			g2.setFont(italic);

			g2.setColor(getBackground().darker().darker());

			int padding = (getHeight() - getFont().getSize()) / 2;
			g2.drawString(hintText, getInsets().left, getHeight() - padding - 1);
			g2.dispose();
		}
	}

	public String getHint() {
		return hintText;
	}

	@Override
	public void focusLost(FocusEvent e) {
		repaint();
	}

	@Override
	public void focusGained(FocusEvent e) {
		repaint();
	}
}