package rgt.ui.common;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class RowPanel extends JPanel {
	
	private GridBagConstraints constraints;
	
	public RowPanel() {
		uiLayout();
	}
	
	private void uiLayout() {
		setOpaque(false);
		setLayout(new GridBagLayout());
		constraints = new GridBagConstraints();
		constraints.insets = new Insets(5, 5, 5, 5);
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.fill = GridBagConstraints.NONE;
		constraints.gridy = 0;
	}

	protected void addRow(JComponent field) {
		constraints.gridx = 0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(field, constraints);
		field.setPreferredSize(new Dimension(200, 30));
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;
		constraints.gridy += 1;
	}

	protected void addFieldRow(JLabel label, JComponent field) {
		constraints.gridx = 0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(label, constraints);
		constraints.fill = GridBagConstraints.NONE;
		constraints.gridx++;

		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(field, constraints);
		field.setPreferredSize(new Dimension(200, 30));
		constraints.fill = GridBagConstraints.NONE;
		constraints.weightx = 0.0;
		
		constraints.gridy += 1;
	}

}
