package rgt.ui.common;

import javax.swing.table.DefaultTableModel;

public class UneditableTableModel extends DefaultTableModel {

	@Override
	public boolean isCellEditable(int row, int column) {
		return false;
	}
}
