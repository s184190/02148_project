package rgt.ui.common;

import java.awt.Component;

import javax.swing.JOptionPane;

public class PopUp {
	public static void error(String title, String message, Component parent) {
		JOptionPane.showMessageDialog(parent, message, title, JOptionPane.ERROR_MESSAGE);
	}
	
	public static void info(String title, String message, Component parent) {
		JOptionPane.showMessageDialog(parent, message, title, JOptionPane.PLAIN_MESSAGE);
	}
}
