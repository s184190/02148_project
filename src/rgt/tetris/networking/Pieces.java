package rgt.tetris.networking;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Pieces {
	
	private Map<String, ArrayList<Point>> piecesWithOrientation;
	private ArrayList<Piece> pieces;
	
	public Pieces() {
		
		piecesWithOrientation = new HashMap<String, ArrayList<Point>>();
		piecesWithOrientation.put("I_0", loadPiece(I_0));
		piecesWithOrientation.put("I_90", loadPiece(I_90));
		piecesWithOrientation.put("I_180", loadPiece(I_180));
		piecesWithOrientation.put("I_270", loadPiece(I_270));

		piecesWithOrientation.put("O_0", loadPiece(O));
		piecesWithOrientation.put("O_90", loadPiece(O));
		piecesWithOrientation.put("O_180", loadPiece(O));
		piecesWithOrientation.put("O_270", loadPiece(O));

		piecesWithOrientation.put("T_0", loadPiece(T_0));
		piecesWithOrientation.put("T_90", loadPiece(T_90));
		piecesWithOrientation.put("T_180", loadPiece(T_180));
		piecesWithOrientation.put("T_270", loadPiece(T_270));

		piecesWithOrientation.put("S_0", loadPiece(S_0));
		piecesWithOrientation.put("S_90", loadPiece(S_90));
		piecesWithOrientation.put("S_180", loadPiece(S_180));
		piecesWithOrientation.put("S_270", loadPiece(S_270));

		piecesWithOrientation.put("Z_0", loadPiece(Z_0));
		piecesWithOrientation.put("Z_90", loadPiece(Z_90));
		piecesWithOrientation.put("Z_180", loadPiece(Z_180));
		piecesWithOrientation.put("Z_270", loadPiece(Z_270));

		piecesWithOrientation.put("J_0", loadPiece(J_0));
		piecesWithOrientation.put("J_90", loadPiece(J_90));
		piecesWithOrientation.put("J_180", loadPiece(J_180));
		piecesWithOrientation.put("J_270", loadPiece(J_270));

		piecesWithOrientation.put("L_0", loadPiece(L_0));
		piecesWithOrientation.put("L_90", loadPiece(L_90));
		piecesWithOrientation.put("L_180", loadPiece(L_180));
		piecesWithOrientation.put("L_270", loadPiece(L_270));
		

		pieces = generatePieces();
	}
	
	public ArrayList<Point> get(Piece piece, Orientation orientation) {
		return piecesWithOrientation.get(getPieceAndOrientationTag(piece, orientation));
	}
	

	public ArrayList<Piece> generatePieces() {
		ArrayList<Piece> pieces = new ArrayList<>();
		pieces.add(Piece.I);
		pieces.add(Piece.O);
		pieces.add(Piece.T);
		pieces.add(Piece.S);
		pieces.add(Piece.Z);
		pieces.add(Piece.L);
		pieces.add(Piece.J);

		return pieces;
	}
	
	public ArrayList<Piece> getPieces() {
		return pieces;
	}
	
	public String getPieceAndOrientationTag(Piece piece, Orientation orientation) {
		StringBuilder builder = new StringBuilder();

		switch (piece) {
		case I:
			builder.append("I");
			break;
		case J:
			builder.append("J");
			break;
		case L:
			builder.append("L");
			break;
		case O:
			builder.append("O");
			break;
		case S:
			builder.append("S");
			break;
		case T:
			builder.append("T");
			break;
		case Z:
			builder.append("Z");
			break;
		}

		switch (orientation) {
		case _0:
			builder.append("_0");
			break;
		case _90:
			builder.append("_90");
			break;
		case _180:
			builder.append("_180");
			break;
		case _270:
			builder.append("_270");
			break;
		}

		return builder.toString();
	}
	
	public int pieceToBrick(Piece piece) {
		switch (piece) {
		case I:
			return 1;
		case O:
			return 2;
		case T:
			return 3;
		case S:
			return 4;
		case Z:
			return 5;
		case J:
			return 6;
		case L:
			return 7;
		default:
			throw new IllegalStateException("Invalid piece: " + piece);
		}
	}

	public ArrayList<Point> loadPiece(String piece) {
		String noWhiteSpaces = piece.replace(" ", "");

		ArrayList<Point> points = new ArrayList<>();
		for (int i = 0; i < noWhiteSpaces.length(); i++) {
			char character = noWhiteSpaces.charAt(i);

			if (character == 'o') {
				int x = i % 4;
				int y = 4 - i / 4;
				points.add(new Point(x, y));
			}
		}

		return points;
	}
	
	//=== I ===
	private String I_0 = 
			". . . ." + 
			"o o o o" + 
			". . . ." + 
			". . . .";

	private String I_90 = 
			". . o ." + 
			". . o ." + 
			". . o ." + 
			". . o .";

	private String I_180 = 
			". . . ." + 
			". . . ." + 
			"o o o o" + 
			". . . .";

	private String I_270 = 
			". o . ." + 
			". o . ." + 
			". o . ." + 
			". o . .";

	// === O ===
	private String O = 
			". . . ." + 
			". o o ." + 
			". o o ." + 
			". . . .";

	// === T ===
	private String T_0 = 
			". . . ." + 
			". o . ." + 
			"o o o ." + 
			". . . .";

	private String T_90 = 
			". . . ." + 
			". o . ." + 
			". o o ." + 
			". o . .";

	private String T_180 = 
			". . . ." + 
			". . . ." + 
			"o o o ." + 
			". o . .";

	private String T_270 = 
			". . . ." + 
			". o . ." + 
			"o o . ." + 
			". o . .";

	// === S ===
	private String S_0 = 
			". . . ." + 
			". o o ." + 
			"o o . ." + 
			". . . .";

	private String S_90 = 
			". . . ." + 
			". o . ." + 
			". o o ." + 
			". . o .";

	private String S_180 = 
			". . . ." + 
			". . . ." + 
			". o o ." + 
			"o o . .";

	private String S_270 =
			". . . ." + 
			"o . . ." + 
			"o o . ." + 
			". o . .";

	// === Z ===
	private String Z_0 = 
			". . . ." + 
			"o o . ." + 
			". o o ." + 
			". . . .";

	private String Z_90 = 
			". . . ." + 
			". . o ." + 
			". o o ." + 
			". o . .";
	
	private String Z_180 = 
			". . . ." + 
			". . . ." + 
			"o o . ." + 
			". o o .";

	private String Z_270 = 
			". . . ." + 
			". o . ." + 
			"o o . ." + 
			"o . . .";

	// === J ===
	private String J_0 = 
			". . . ." + 
			"o . . ." + 
			"o o o ." + 
			". . . .";

	private String J_90 = 
			". . . ." + 
			". o o ." + 
			". o . ." + 
			". o . .";

	private String J_180 = 
			". . . ." + 
			". . . ." + 
			"o o o ." + 
			". . o .";

	private String J_270 = 
			". . . ." + 
			". o . ." + 
			". o . ." + 
			"o o . .";

	// === L ===
	private String L_0 = 
			". . . ." + 
			". . o ." + 
			"o o o ." + 
			". . . .";

	private String L_90 = 
			". . . ." + 
			". o . ." + 
			". o . ." + 
			". o o .";

	private String L_180 = 
			". . . ." + 
			". . . ." + 
			"o o o ." + 
			"o . . .";

	private String L_270 = 
			". . . ." + 
			"o o . ." + 
			". o . ." + 
			". o . .";
}
