package rgt.tetris.networking;

// wrapper to make Integer parse by referece instead of value ("Integer" does not provide this).
public class IntegerRef {
	
	public int value;
	
	public IntegerRef(int value) {
		this.value = value;
	}

}
