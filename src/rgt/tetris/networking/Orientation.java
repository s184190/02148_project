package rgt.tetris.networking;

public enum Orientation {
	_0,
	_90,
	_180,
	_270;
}
