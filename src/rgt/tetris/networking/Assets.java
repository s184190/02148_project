package rgt.tetris.networking;

import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

import engine.Utils;

public class Assets {
	
	private BufferedImage I_BRICK = Utils.loadImage("/I_brick.png");
	private BufferedImage O_BRICK = Utils.loadImage("/O_brick.png");
	private BufferedImage T_BRICK = Utils.loadImage("/T_brick.png");
	private BufferedImage S_BRICK = Utils.loadImage("/S_brick.png");
	private BufferedImage Z_BRICK = Utils.loadImage("/Z_brick.png");
	private BufferedImage L_BRICK = Utils.loadImage("/L_brick.png");
	private BufferedImage J_BRICK = Utils.loadImage("/J_brick.png");
	private BufferedImage DEBT_BRICK = Utils.loadImage("/Black_brick.png");
	
	private Map<Integer, BufferedImage> typeToBrickImage = new HashMap<>();
	
	public Assets() {
		typeToBrickImage.put(1, I_BRICK);
		typeToBrickImage.put(2, O_BRICK);
		typeToBrickImage.put(3, T_BRICK);
		typeToBrickImage.put(4, S_BRICK);
		typeToBrickImage.put(5, Z_BRICK);
		typeToBrickImage.put(6, J_BRICK);
		typeToBrickImage.put(7, L_BRICK);
		typeToBrickImage.put(8, DEBT_BRICK);
	}
	
	public BufferedImage getBrick(int value){
		return typeToBrickImage.get(value);
	}
	
}
