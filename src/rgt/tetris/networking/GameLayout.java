package rgt.tetris.networking;

import java.util.ArrayList;
import java.util.List;

import engine.math.MathUtils;
import engine.math.Vector2;

public class GameLayout {

	private BoardLayout mainBoardLayout;
	private List<BoardLayout> subBoardLayouts;

	private int yourTileSize;
	private int othersTileSize;
	
	private Vector2 layoutDimensions;
	
	public GameLayout(int yourBoardIndex, int numberOfBoards, int boardWidth, int boardHeight) {
		subBoardLayouts = new ArrayList<>();
		
		yourTileSize = 24;
		othersTileSize = 8;
		
		{
			BoardLayout yourLayout = new BoardLayout();
			yourLayout.boardIndex = yourBoardIndex;
			yourLayout.x = 0;
			yourLayout.y = 0;
			yourLayout.tilesize = yourTileSize;
			mainBoardLayout = yourLayout;
		}

		int maxPieceLength = 4;
		int gapSubBoardsX = 60;
		int gapMainBoardsX = yourTileSize * (maxPieceLength + 1);
		
		int yourHeight = boardHeight * yourTileSize;
		int yourWidth = boardWidth * yourTileSize;
		
		int otherHeight = boardHeight * othersTileSize;
		int otherWidth = boardWidth * othersTileSize;

		// topRow
		int otherBoards = numberOfBoards - 1;
		int topRow = MathUtils.ceil(otherBoards / 2f);
		
		List<Integer> topRowIndices = getTopRowIndices(yourBoardIndex, numberOfBoards, topRow);
		for (int i = 0; i < topRow; i++) {
			BoardLayout layout = new BoardLayout();
			layout.tilesize = othersTileSize;
			
			layout.x = yourWidth + otherWidth * i + gapSubBoardsX * i + gapMainBoardsX;
			layout.y = yourHeight - otherHeight;

			layout.boardIndex = topRowIndices.get(i);
			subBoardLayouts.add(layout);
		}
		
		// bottomRow
		int bottomRow = MathUtils.floor(otherBoards / 2f);
		
		List<Integer> bottomRowIndices = getBottomRowIndices(yourBoardIndex, numberOfBoards, bottomRow);
		for (int i = 0; i < bottomRow; i++) {
			BoardLayout layout = new BoardLayout();
			layout.tilesize = othersTileSize;
			
			layout.x = yourWidth + otherWidth * i + gapSubBoardsX * i + gapMainBoardsX;
			layout.y = 0;
			
			layout.boardIndex = bottomRowIndices.get(i);
			subBoardLayouts.add(layout);
		}
		
		layoutDimensions = new Vector2();
		layoutDimensions.x = (yourWidth + topRow * otherWidth + gapSubBoardsX * topRow + gapMainBoardsX);
		layoutDimensions.y = yourHeight;
	}
	
	private List<Integer> getBottomRowIndices(int yourBoardIndex, int numberOfBoards, int bottomRow) {
		List<Integer> boardsIndices = new ArrayList<>();
		
		for (int i = 1; i <= bottomRow; i++) {
			int boardIndex = MathUtils.wrap(yourBoardIndex - i, numberOfBoards);
			boardsIndices.add(boardIndex);
		}
		return boardsIndices;
	}
	
	private List<Integer> getTopRowIndices(int yourBoardIndex, int numberOfBoards, int topRow) {
		List<Integer> boardsIndices = new ArrayList<>();
		
		for (int i = 1; i <= topRow; i++) {
			int boardIndex = MathUtils.wrap(yourBoardIndex + i, numberOfBoards);
			boardsIndices.add(boardIndex);
		}
		return boardsIndices;
	}
	
	public List<BoardLayout> getSubBoardLayouts() {
		return subBoardLayouts;
	}
	
	public BoardLayout getMainBoardLayout() {
		return mainBoardLayout;
	}
	
	public Vector2 getLayoutDimensions() {
		return layoutDimensions;
	}
	
	public int getMainBoardTilesize() {
		return yourTileSize;
	}

}
