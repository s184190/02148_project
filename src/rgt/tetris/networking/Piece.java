package rgt.tetris.networking;

public enum Piece {
	I,
	O,
	T,
	S, 
	Z,
	J,
	L;
}
