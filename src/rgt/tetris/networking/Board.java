package rgt.tetris.networking;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Board {
	
	public float fallTick;
	public float ticksToFall;

	public float solidifyTick;
	public float ticksToSolidify;

	public int width;
	public int height;
	public int grid[][];

	public int looseHeight;
	
	public boolean dead;

	public Orientation orientation;
	public Piece piece;
	public Point position = new Point();

	public Piece holdingPiece;
	public boolean swappedPiece;
	
	public ArrayList<Piece> nextPieces;
	
	public Random pieceRandomGenerator;
	public Random debtRandomGenerator;
	
	public List<IntegerRef> debtChunks;
	public int totalDebt;
	
	public Board(int width, int height, int looseHeight, long pieceSeed, long debtSeed) {
		this.width = width;
		this.height = height;
		this.looseHeight = looseHeight;
		
		position = new Point();
		grid = new int[width][height];
		
		fallTick = 0;
		ticksToFall = 1.5f;
		
		solidifyTick = 0;
		ticksToSolidify = 0.5f;
		
		swappedPiece = false;
		
		debtChunks = new ArrayList<>();
		totalDebt = 0;
		
		nextPieces = new ArrayList<Piece>();
		
		pieceRandomGenerator = new Random(pieceSeed);
		debtRandomGenerator = new Random(debtSeed);
	}
}
