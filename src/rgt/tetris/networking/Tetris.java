package rgt.tetris.networking;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import engine.math.MathUtils;

public class Tetris {

	public Pieces pieces;
	
	private int numberOfNextPieces;
	private int maxPieceLength;
	
	public List<Board> boards;
	
	private int boardWidth;
	private int boardHeight;
	private int boardLooseHeight;
	
	private List<Point> rotationCandidates;
	
	private List<Board> boardDeathOrder;
	
	private int numberOfBoardsDead;
	
	// @todo: start boards!!!
	
	public Tetris() {
		pieces = new Pieces();
		boardWidth = 10;
		boardHeight = 22;
		boardLooseHeight = 20;
		
		boardDeathOrder = new ArrayList<>();
		
		numberOfNextPieces = 3;
		maxPieceLength = 4;
		
		numberOfBoardsDead = 0;
		
		// sometimes we can not rotate in out current position,
		// so we try some deltas. This makes t-spin easier.
		rotationCandidates = new ArrayList<>();
		rotationCandidates.add(new Point(0, 0));
		rotationCandidates.add(new Point(0, -1));
		rotationCandidates.add(new Point(0, -2));
		rotationCandidates.add(new Point(-1, -1));
		rotationCandidates.add(new Point(+1, -1));
		rotationCandidates.add(new Point(-1, 0));
		rotationCandidates.add(new Point(-2, 0));
		rotationCandidates.add(new Point(+1, 0));
		rotationCandidates.add(new Point(+2, 0));
		rotationCandidates.add(new Point(0, 1));
		rotationCandidates.add(new Point(0, 2));
		rotationCandidates.add(new Point(-1, +1));
		rotationCandidates.add(new Point(+1, +1));
	}
	
	// client init
	public void populate(List<Board> boards) {
		this.boards = boards;
	}
	
	// server init.
	public void create(int numberOfPlayers, long pieceSeed, long debtSeed) {
		boards = new ArrayList<>();
		
		for (int i = 0; i < numberOfPlayers; i++) {
			Board board = new Board(boardWidth, boardHeight, boardLooseHeight, pieceSeed, debtSeed);
			
			generateNextPieces(board);
			
			Piece piece = consumeNextPiece(board);
			setCurrentPiece(piece, board);
			generatePiece(board);
			
			boards.add(board);
		}
	}
	
	private void generateNextPieces(Board board) {
		for(int i = 0; i < numberOfNextPieces; i++) {
			generatePiece(board);
		}
	}
	
	private void generatePiece(Board board) {
		Piece nextPiece = randomGeneratePiece(board);
		board.nextPieces.add(nextPiece);
	}
	
	private Piece consumeNextPiece(Board board) {
		Piece nextPiece = board.nextPieces.remove(0);
		if (nextPiece == null) throw new IllegalStateException("next piece queue is empty!");
		return nextPiece;
	}
	
	private void setCurrentPiece(Piece piece, Board board) {
		board.position.x = board.width / 2 - 1;
		board.position.y = board.looseHeight - maxPieceLength;
		
		board.orientation = Orientation._0;
		board.piece = piece;

		board.fallTick = 0;
		
		int yBuffer = 2;
		for (int i = 0; i < yBuffer; i++) {
			if (isHittingBoardPieces(board.position.x, board.position.y, board.piece, board.orientation, board)) {
				board.position.y += 1;
			}
		}
	}
	
	private void holdPiece(Board board) {
		
		if (!board.swappedPiece) {
			board.swappedPiece = true;
			board.solidifyTick = 0;

			if (board.holdingPiece == null) {
				board.holdingPiece = board.piece;
				
				Piece nextPiece = consumeNextPiece(board);
				setCurrentPiece(nextPiece, board);
				generatePiece(board);
				
			} else {
				// swap
				Piece holdingPiece = board.holdingPiece;
				board.holdingPiece = board.piece;
				
				setCurrentPiece(holdingPiece, board);
			}
		}
	}
	
	public void hold(Board board) {
		holdPiece(board);
	}
	
	public void down(Board board) {
		// reset fallTick when pressing down so we do get weird "double fall".
		board.fallTick = 0;
		move(0, -1, board);
	}
	
	public void space(Board board) {
		drop(board);
		
		solidify(board);
		
		board.swappedPiece = false;
		
		int rowsRemoved = repeatClearAndCollapsRows(board);
		handleDebt(rowsRemoved, board);
		checkBoardDead(board);
		
		Piece nextPiece = consumeNextPiece(board);
		setCurrentPiece(nextPiece, board);
		generatePiece(board);
	}
	
	private void checkBoardDead(Board board) {
		if (board.dead) return;	// if board is dead. No need to do the check.

		if (isBoardDead(board)) {
			board.dead = true;
			numberOfBoardsDead += 1;
			boardDeathOrder.add(board);
		}
	}

	public void onSolidify(Board board) {
		solidify(board);
		
		int rowsRemoved = repeatClearAndCollapsRows(board);
		handleDebt(rowsRemoved, board);
		checkBoardDead(board);
		
		board.swappedPiece = false;
		
		Piece nextPiece = consumeNextPiece(board);
		setCurrentPiece(nextPiece, board);
		generatePiece(board);
	}
	
	public void left(Board board) {
		move(-1, 0, board);
	}
	
	public void right(Board board) {
		move(1, 0, board);
	}
	
	public boolean rotate(Board board) {
		Orientation orientation = board.orientation;
		
		Orientation newOrientation;
		if (orientation == Orientation._0)
			newOrientation = Orientation._90;
		else if (orientation == Orientation._90)
			newOrientation = Orientation._180;
		else if (orientation == Orientation._180)
			newOrientation = Orientation._270;
		else
			newOrientation = Orientation._0;

		for (int i = 0; i < rotationCandidates.size(); i++) {
			Point delta = rotationCandidates.get(i);
			int nx = board.position.x + delta.x;
			int ny = board.position.y + delta.y;

			boolean validRotation = !isHittingWall(nx, ny, newOrientation, board) && !isHittingBoardPieces(nx, ny, board.piece, newOrientation, board);
			if (validRotation) {
				if (validRotation) {
					board.position.x = nx;
					board.position.y = ny;
					board.orientation = newOrientation;
					return true;
				}
			}
		}
		
		return false;
	}
	
	public void fallTick(float dt, Board board) {
		board.fallTick += dt;
	}
	
	public boolean shouldFall(Board board) {
		return board.fallTick >= board.ticksToFall;
	}
	
	public void solidifyTick(float dt, Board board) {
		if (isTouchingGround(board) || (!isTouchingGround(board) && isHittingBoardPieces(board.position.x, board.position.y - 1, board.piece, board.orientation, board))) {
			board.solidifyTick += dt;
		}
	}
	
	private void handleDebt(int rowsRemoved, Board board) {
		int debtLeft = payDebt(rowsRemoved, board);
		
		if (debtLeft != 0) {
			int lines = Math.abs(debtLeft);
			if (debtLeft > 0) {
				commitAllDebtChunks(board);
			} else {
				Board victim = getVictimBoard(board);
				addDebt(lines, victim);
			}
		}
	}
	
	private void commitAllDebtChunks(Board board) {
		
		for (IntegerRef debt : board.debtChunks) {
			pushRowsUp(debt.value, board);
			addDebtRows(debt.value, board);
		}
		
		board.totalDebt = 0;
		board.debtChunks.clear();
	}
	
	private void pushRowsUp(int lines, Board board) {
		for (int i = 0; i < lines; i++) {
			pushRowsOneUp(lines, board);
		}
	}
	
	private void addDebtRows(int debt, Board board) {
		Random randomGenerator = board.debtRandomGenerator;
		
		double random = randomGenerator.nextDouble();
		int xBrickToRemove = (int) (random * board.width);
		
		int debtBrick = 8;
		for (int y = 0; y < debt; y++) {
			fillRow(y, debtBrick, board);
			
			// remove a brick
			board.grid[xBrickToRemove][y] = 0;
		}
	}
	
	private void fillRow(int y, int value, Board board) {
		int[][] grid = board.grid;
		
		for (int x = 0; x < board.width; x++) {
			grid[x][y] = value;
		}
	}

	private Board getVictimBoard(Board board) {
		int boardId = getBoardId(board);
		
		int numberOfBoards = boards.size();
		for (int i = 0; i < numberOfBoards; i++) {
			int offset = i + 1;
			int potientialVictimId = MathUtils.wrap(boardId + offset, numberOfBoards);
			
			Board victimBoard = getBoard(potientialVictimId);
			if (!victimBoard.dead) return victimBoard;
			
		}
		
		throw new IllegalStateException("no victim board found...");
	}
	
	private void addDebt(int debt, Board board) {
		board.debtChunks.add(new IntegerRef(debt));
		board.totalDebt += debt;
	}

	private void pushRowsOneUp(int lines, Board board) {
		for(int y = boardLooseHeight; y >= 0; y--) {
			copyRow(y, y+1, board);
		}
	}

	private int payDebt(int debtPayed, Board board) {

		// remove debt chunks.
		int debtPayLeft = debtPayed;
		for (Iterator<IntegerRef> it = board.debtChunks.iterator(); it.hasNext();) {
			IntegerRef debt = (IntegerRef) it.next();
			
			if (debtPayLeft >= debt.value) {
				debtPayLeft -= debt.value;
				it.remove();
			} else {
				debt.value -= debtPayLeft;
				break;
			}
		}
		
		int debtLeft = board.totalDebt - debtPayed;
		board.totalDebt = Math.max(debtLeft, 0);

		return debtLeft;
	}

	public boolean shouldSolidify(Board board) {
		return board.solidifyTick >= board.ticksToSolidify;
	}

	private int repeatClearAndCollapsRows(Board board) {
		int totalRowsCleared = 0;
		
		int rowsCleared = clearAllFullRow(board);
		while (rowsCleared > 0) {
			totalRowsCleared += rowsCleared;
			
			collapseRows(board);
			rowsCleared = clearAllFullRow(board);
		}
		
		return totalRowsCleared;
	}
	
	private void drop(Point position, Board board) {
		boolean tryToFall = move(0, -1, position, board);
		while (tryToFall) {
			tryToFall = move(0, -1, position, board);
		}
	}
	
	private void drop(Board board) {
		drop(board.position, board);
	}

	private boolean isBoardDead(Board board) {
		for (int x = 0; x < board.width; x++) {
			if (board.grid[x][board.looseHeight] != 0)
				return true;
		}

		return false;
	}

	public boolean isRowEmpty(int y, Board board) {
		for (int x = 0; x < board.width; x++) {
			if (board.grid[x][y] != 0)
				return false;
		}
		return true;
	}

	private void collapseRows(Board board) {
		for (int y = 1; y <  board.height; y++) {

			int yEnd = y;
			while (yEnd > 0) {
				if (!isRowEmpty(yEnd - 1, board))
					break;
				yEnd -= 1;
			}

			if (yEnd != y) {
				copyRow(y, yEnd, board);
				clearRow(y, board);
			}
		}
	}

	private void copyRow(int ySrc, int yDst, Board board) {
		int[][] grid = board.grid;
		
		for (int x = 0; x < board.width; x++) {
			grid[x][yDst] = grid[x][ySrc];
		}
	}

	private void clearRow(int y, Board board) {
		int[][] grid = board.grid;
		
		for (int x = 0; x < board.width; x++) {
			grid[x][y] = 0;
		}
	}

	private int clearAllFullRow(Board board) {
		int rowsCleared = 0;
		for (int y = 0; y < board.height; y++) {

			boolean removeRow = true;
			for (int x = 0; x < board.width; x++) {
				if (board.grid[x][y] == 0) {
					removeRow = false;
					break;
				}
			}

			if (removeRow) {
				rowsCleared += 1;
				clearRow(y, board);
			}
		}

		return rowsCleared;
	}

	private Piece randomGeneratePiece(Board board) {
		Random randomGenerator = board.pieceRandomGenerator;
		
		double random = randomGenerator.nextDouble();

		int numberOfPieces = pieces.getPieces().size();
		int randomPiece = (int) (random * numberOfPieces);
		Piece piece = pieces.getPieces().get(randomPiece);
		return piece;
	}

	private void solidify(Board board) {
		Piece piece = board.piece;
		Orientation orientation = board.orientation;
		Point position = board.position;
		
		int brick = pieces.pieceToBrick(piece);

		ArrayList<Point> points = pieces.get(piece, orientation);
		for (Point localSpace : points) {
			int nx = position.x + localSpace.x;
			int ny = position.y + localSpace.y;
			board.grid[nx][ny] = brick;
		}

		board.solidifyTick = 0;
	}

	private boolean isHittingWall(int x, int y, Orientation orientation, Board board) {
		
		ArrayList<Point> points = pieces.get(board.piece, orientation);
		
		for (Point localSpace : points) {
			int nx = x + localSpace.x;
			int ny = y + localSpace.y;

			if (nx < 0 || nx > board.width - 1 || ny < 0 || ny > board.height - 1) {
				return true;
			}
		}

		return false;
	}
	
	private boolean move(int dx, int dy, Point position, Board board) {
		Orientation orientation = board.orientation;
		Piece piece = board.piece;
		
		int nx = position.x + dx;
		int ny = position.y + dy;

		boolean validMove = !isHittingWall(nx, ny, orientation, board) && !isHittingBoardPieces(nx, ny, piece, orientation, board);
		if (validMove) {
			position.x = nx;
			position.y = ny;
			
			return true;
		}
		
		return false;
	}

	
	private boolean move(int dx, int dy, Board board) {
		return move(dx, dy, board.position, board);
	}

	private boolean isTouchingGround(Board board) {
		Point position = board.position;
		Piece piece = board.piece;
		Orientation orientation = board.orientation;
		
		ArrayList<Point> points = pieces.get(piece, orientation);

		for (Point localSpace : points) {
			int ny = position.y + localSpace.y;
			if (ny == 0) return true;
		}

		return false;
	}

	private boolean isHittingBoardPieces(int x, int y, Piece piece, Orientation orientation, Board board) {
		int[][] grid = board.grid;
		
		ArrayList<Point> points = pieces.get(piece, orientation);

		for (Point localSpace : points) {
			int nx = x + localSpace.x;
			int ny = y + localSpace.y;
			if (grid[nx][ny] != 0) {
				return true;
			}
		}

		return false;
	}

	public Board getBoard(int index) {
		return boards.get(index);
	}

	public void fall(Board board) {
		board.fallTick -= board.ticksToFall;
		move(0, -1, board);
	}

	public int getWidth() {
		return boardWidth;
	}
	
	public int getHeight() {
		return boardLooseHeight;	// visualHeight.
	}
	
	public int getNumberOfBoards() {
		return boards.size();
	}

	public List<Board> getBoards() {
		return boards;
	}
	
	private Point ghostPosition = new Point();
	
	public Point getGhostPosition(Board board) {
		ghostPosition.x = board.position.x;
		ghostPosition.y = board.position.y;
		drop(ghostPosition, board);
		return ghostPosition;
	}
	
	public boolean gameOver() {
		int boardsAlive = getNumberOfBoards() - numberOfBoardsDead;
		return (boardsAlive == 1);
	}
	
	private Board getWinningBoard() {
		if (!gameOver()) throw new IllegalStateException("no winning board yet");
		
		for (Board board : boards) {
			if (!board.dead) return board;
		}
		
		throw new IllegalStateException("can never happen!");
	}

	private List<Board> getRankingOrder() {
		List<Board> winningOrder = new ArrayList<>();
		winningOrder.addAll(boardDeathOrder);

		Board winner = getWinningBoard();
		winningOrder.add(winner);

		Collections.reverse(winningOrder);
		
		return winningOrder;
	}
	
	/**
	 * if there are less top boards than requested top ranking, 
	 * then just early stop.
	 *	 
	 * @param numberOfTopBoards
	 * @return topRankingBoards
	 */
	
	public List<Board> getTopRankings(int numberOfTopBoards) {
		if (numberOfTopBoards == 0) throw new IllegalStateException("invalid value!");
		
		List<Board> rankingOrder = getRankingOrder();
		List<Board> topRankingBoards = new ArrayList<>();
		
		int rankingAt = 0;
		for (Board board : rankingOrder) {
			rankingAt += 1;
			topRankingBoards.add(board);

			if (rankingAt == numberOfTopBoards) break;
		}
		
		return topRankingBoards;
	}
	
	public int getBoardId(Board board) {
		return boards.indexOf(board);
	}
	
	public int maxPieceLength () {
		return maxPieceLength;
	}
}
