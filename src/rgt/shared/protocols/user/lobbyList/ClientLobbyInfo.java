package rgt.shared.protocols.user.lobbyList;

import java.util.ArrayList;
import java.util.List;

public class ClientLobbyInfo {
	public int lid;
	public String name;
	public String owner;
	public List<String> players = new ArrayList<>(); // inclusive owner
	
	public int maxNumberOfPlayers;
	public int minNumberOfPlayersToStartGame;
}
