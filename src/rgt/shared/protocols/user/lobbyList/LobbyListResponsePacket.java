package rgt.shared.protocols.user.lobbyList;

import java.util.ArrayList;
import java.util.List;

public class LobbyListResponsePacket {
	public List<ClientLobbyInfo> lobbies = new ArrayList<>();
}
