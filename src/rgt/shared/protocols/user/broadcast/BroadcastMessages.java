package rgt.shared.protocols.user.broadcast;

public enum BroadcastMessages {
	start,
	terminate, 
	leave, 
	join;
}
