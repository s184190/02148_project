package rgt.shared.protocols.user.userInfo;

public class UserInfoPacket {
	
	public int uid;
	public String username;
	
	public boolean inGame;
	public String gameSpacePath;

	public int firstPlaces;
	public int secondPlaces;
	public int thirdPlaces;
	public int gamesPlayed;
}
