package rgt.shared.protocols.user.userInfo;

public enum UserMessages {
	userInfo,
	createLobby, 
	startGame,
	terminateLobby,
	lobbyList, 
	joinLobby, 
	leaveLobby;
}
