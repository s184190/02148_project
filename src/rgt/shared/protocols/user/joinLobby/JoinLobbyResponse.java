package rgt.shared.protocols.user.joinLobby;

import rgt.shared.protocols.user.lobbyList.ClientLobbyInfo;

public class JoinLobbyResponse {
	public boolean success;
	public ClientLobbyInfo info;
}
