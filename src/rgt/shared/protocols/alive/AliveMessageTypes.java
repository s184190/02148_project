package rgt.shared.protocols.alive;

public class AliveMessageTypes {
	public static final String aliveResponse = "i am alive";
	public static final String aliveRequest = "are you alive?";
}
