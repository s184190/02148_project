package rgt.shared.protocols.users.lobbyCreation;

import rgt.shared.protocols.user.lobbyList.ClientLobbyInfo;

public class LobbyCreationResponsePacket {
	public boolean created;
	public ClientLobbyInfo lobbyInfo = new ClientLobbyInfo();
}
