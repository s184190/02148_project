package rgt.shared.protocols.game;

public enum ServerGameMessages {
	left, 
	right,
	down,
	space,
	hold,
	rotate,
	solidify,
	fall,
	gameOver;
}
