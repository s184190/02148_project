package rgt.shared.protocols.game;

public enum ClientGameMessages {
	left, 
	right,
	down,
	space,
	hold,
	rotate;
}
