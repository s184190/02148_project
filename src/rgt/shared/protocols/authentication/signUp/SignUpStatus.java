package rgt.shared.protocols.authentication.signUp;

public enum SignUpStatus {
	success,
	weakPassword,
	unavailableUsername,
	disconnected,
	notConnected;
}
