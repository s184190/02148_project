package rgt.shared.protocols.authentication.signUp;

public class SignUpServerPacket {
	public int uid;
	public String username;
	public SignUpStatus status;
}
