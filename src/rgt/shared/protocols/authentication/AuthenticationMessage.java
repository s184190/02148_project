package rgt.shared.protocols.authentication;

public enum AuthenticationMessage {
	signUp,
	signIn,
	signOut;
}
