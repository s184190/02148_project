package rgt.shared.protocols.authentication.signIn;

public enum SignInStatus {
	success,
	userNotFound,
	disconnected, 
	alreadyLoggedIn,
	notConnected;
}
