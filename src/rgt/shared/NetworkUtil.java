package rgt.shared;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class NetworkUtil {
	
	public static final boolean useLocalHost = false;
	public static String localhost = "localhost";

	private static int port = 11111;
	private static String mode = "conn";
	private static String protocol = "tcp";
	private static String gateTemplate = "%s://%s:%d/%s?%s";
	
	public static String getGate(String address, String space) {
		return String.format(gateTemplate, protocol, address, port, space, mode);
	}
	
	public static String getIP4Address() {
		if (useLocalHost) return localhost;
		
		String ip = null;

		try {
			DatagramSocket socket = new DatagramSocket();
			socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
			ip = socket.getLocalAddress().getHostAddress();
			socket.close();
		} catch (SocketException | UnknownHostException e) {
		}

		return ip;
	}
	
	public static final String authenticationSpaceId = "authentication";
	public static final String tetrisSpaceId = "user";
	public static final String identitySpaceId = "identity";
	public static final String aliveSpaceId = "alive";
	public static final String authSafeId = "signUpServerNoticeSpace";
	public static final String gameSpaceId = "gameSpace";
	public static final String gameEndingSpaceId = "gameEnding";	// one space for all gameserver<->server terminate communication.
}
