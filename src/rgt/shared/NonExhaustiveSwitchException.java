package rgt.shared;

public class NonExhaustiveSwitchException extends IllegalStateException {

	public NonExhaustiveSwitchException() {
		this("");
	}
	
	public NonExhaustiveSwitchException(String message) {
		super(message);
	}
}
