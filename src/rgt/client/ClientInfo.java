package rgt.client;

import rgt.shared.protocols.user.lobbyList.ClientLobbyInfo;

public class ClientInfo {
	public boolean connected;
	public String ip4;
	
	public int uid;
	public String username;
	
	public boolean loggedIn;
	
	public boolean inLobby;
	public boolean inGame;
	
	public String myPublicKey;
	public String myPrivateKey;
	
	public String serverPublicKey;
	
	public ClientLobbyInfo lobbyInfo = new ClientLobbyInfo();
	public ClientGameInfo gameInfo = new ClientGameInfo();
	
	public int gamesPlayed;
	public int firstPlaces;
	public int secondPlaces;
	public int thirdPlaces;
}
