package rgt.client;

import engine.Application;
import engine.ApplicationConfiguration;

public class RunClient {
	
	public static void main(String[] args) {
		ApplicationConfiguration config = new ApplicationConfiguration();
		config.width = 720;
		config.height = 600;
		config.fps = 60;
		new Application(new ClientApp(), config);
	}
}
