package rgt.client;

public enum ClientState {
	authentication,
	user,
	lobby,
	game;
}
