	package rgt.client.ui;

import java.util.List;

import javax.swing.table.DefaultTableModel;

import rgt.shared.protocols.user.lobbyList.ClientLobbyInfo;
//import rgt.server.identity.IdentityInfo;
import rgt.ui.common.TableListPanel;

public class LobbyListPanel extends TableListPanel {

	private static final int nameColumn = 0;
	private static final int membersColumn = 1;
	
	private static final String[] LOBBY_COLUMN = new String[] {
		"Lobby name:",
		"Lobby members"
	};
	
	public LobbyListPanel() {
		super(LOBBY_COLUMN);
	}
	
	public void updateCells(List<ClientLobbyInfo> lobbyInfos) {
		DefaultTableModel tableModel = getModel();
		
		int row = 0;
		tableModel.setRowCount(lobbyInfos.size());

		for (ClientLobbyInfo info : lobbyInfos) {
			tableModel.setValueAt(info.name, row, nameColumn);
			
			String playerSpotsOccupied = String.format("%d/%d", info.players.size(), info.maxNumberOfPlayers);
			tableModel.setValueAt(playerSpotsOccupied, row, membersColumn);
			row += 1;
		}
		
		if (tableModel.getRowCount() != 0)
			getTable().getSelectionModel().setSelectionInterval(0, 0);
	}
}
