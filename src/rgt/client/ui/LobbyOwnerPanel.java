package rgt.client.ui;

import java.awt.event.ActionListener;

import javax.swing.JButton;

import rgt.ui.common.RowPanel;

public class LobbyOwnerPanel extends RowPanel {

	private JButton startButton;
	private JButton terminateButton;

	public LobbyOwnerPanel() {
		startButton = new JButton("start");
		terminateButton = new JButton("terminate");
		
		addRow(startButton);
		addRow(terminateButton);
	}

	public void onTerminate(ActionListener actionListener) {
		terminateButton.addActionListener(actionListener);
	}

	public void onStart(ActionListener actionListener) {
		startButton.addActionListener(actionListener);
	}
	
	public void setStartButtonEnabled(boolean enabled) {
		startButton.setEnabled(enabled);
	}
	
	public void setTerminateButtonEnabled(boolean enabled) {
		terminateButton.setEnabled(enabled);
	}
}
