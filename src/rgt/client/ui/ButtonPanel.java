package rgt.client.ui;

import java.awt.event.ActionListener;

import javax.swing.JButton;

import rgt.ui.common.RowPanel;

public class ButtonPanel extends RowPanel {

	private JButton joinButton;
	private JButton leaveButton;
	private JButton refreshButton;

	public ButtonPanel() {
		joinButton = new JButton("Join Lobby");
		leaveButton = new JButton("Leave Lobby");
		refreshButton = new JButton("refresh");
		
		addRow(joinButton);
		addRow(leaveButton);
		addRow(refreshButton);
	}

	public void onRefresh(ActionListener actionListener) {
		refreshButton.addActionListener(actionListener);
	}
	
	public void onLeave(ActionListener actionListener) {
		leaveButton.addActionListener(actionListener);
	}

	public void onJoin(ActionListener actionListener) {
		joinButton.addActionListener(actionListener);
	}
	
	public void setJoinButtonEnable(boolean enable) {
		joinButton.setEnabled(enable);
	}
	
	public void setLeaveButtonEnable(boolean enable) {
		leaveButton.setEnabled(enable);
	}
	
	public void setRefreshButtonEnable(boolean enable) {
		refreshButton.setEnabled(enable);
	}
}
