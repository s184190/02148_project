package rgt.client.ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.table.DefaultTableModel;

import rgt.client.ClientInfo;
import rgt.shared.protocols.user.lobbyList.ClientLobbyInfo;
import rgt.ui.common.Tab;

public class FindLobbyTab extends Tab {
	
	private LobbyListPanel lobbyListPanel;
	private LobbyPanel lobbyInfoPanel;
	private ButtonPanel buttonPanel;
	
	private ClientInfo info;
	private List<ClientLobbyInfo> currentLobbies;
	
	public FindLobbyTab(ClientInfo info, List<ClientLobbyInfo> currentLobbies) {
		this.info = info;
		this.currentLobbies = currentLobbies;
		
		setOpaque(false);

		lobbyListPanel = new LobbyListPanel();
		lobbyListPanel.setBorder(BorderFactory.createTitledBorder("Find Game"));
		
		lobbyInfoPanel = new LobbyPanel();
		lobbyInfoPanel.setBorder(BorderFactory.createTitledBorder("Lobby Info"));
		
		buttonPanel = new ButtonPanel();
		uiLayout();
		uiEvents();

	}
	
	private void uiLayout() {
		setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(5, 5, 5, 5);
		constraints.fill = GridBagConstraints.NONE;
		constraints.anchor = GridBagConstraints.NORTH;
		constraints.weighty = 1.0;

		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(lobbyListPanel, constraints);
		constraints.fill = GridBagConstraints.NONE;
		lobbyListPanel.setOpaque(false);
		

		constraints.gridx = 1;
		constraints.gridy = 0;
		constraints.anchor = GridBagConstraints.NORTH;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(lobbyInfoPanel, constraints);
		lobbyInfoPanel.setOpaque(false);
		
		constraints.gridx = 0;
		constraints.gridy = 1;
		constraints.anchor = GridBagConstraints.WEST;
		constraints.weighty = 0.0;
		buttonPanel.setOpaque(false);
		constraints.weightx = 0.5;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(buttonPanel, constraints);
		
	}

	@Override
	public void enter() {
		refreshList();
		refreshInfoPanel();
	}
	
	@Override
	public void leave() {
	}
	
	public void refreshInfoPanel() {
		lobbyInfoPanel.setLobbyName(info.lobbyInfo.name);
		lobbyInfoPanel.clearPlayerNameFields();
		
		if (info.inLobby) {
			List<String> players = info.lobbyInfo.players;
			for (int i = 0; i < players.size(); i++) {
				String player = players.get(i);
				lobbyInfoPanel.setPlayerNameField(i, player);
			}
		}
	}
	
	public void setJoinListener(ActionListener listener) {
		buttonPanel.onJoin(listener);
	}
	
	public void setLeaveListener(ActionListener listener) {
		buttonPanel.onLeave(listener);
	}
	
	public void setRefreshListener(ActionListener listener) {
		buttonPanel.onRefresh(listener);
	}
	
	public void refreshList() {
			lobbyListPanel.updateCells(currentLobbies);
	}
	
	public ClientLobbyInfo getSelectedClientLobbyInfo() {
		DefaultTableModel tableModel = lobbyListPanel.getModel();
		if (tableModel.getRowCount() <= 0)
			return null;
		
		int index = lobbyListPanel.getTable().getSelectedRow();
		ClientLobbyInfo client = currentLobbies.get(index);
		
		ClientLobbyInfo copy = new ClientLobbyInfo();
		copy.lid = client.lid;
		copy.name = client.name;
		copy.owner = client.owner;
		copy.players = client.players;
		copy.maxNumberOfPlayers = client.maxNumberOfPlayers;
		copy.minNumberOfPlayersToStartGame = client.minNumberOfPlayersToStartGame;
		
		return copy;
	}

	public void lockJoinButton() {
		buttonPanel.setJoinButtonEnable(false);
	}
	
	public void unlockJoinButton() {
		buttonPanel.setJoinButtonEnable(true);
	}
	
	public void lockRefreshButton() {
		buttonPanel.setRefreshButtonEnable(false);
	}
	
	public void unlockRefreshButton() {
		buttonPanel.setRefreshButtonEnable(true);
	}
	
	public void lockLeaveButton() {
		buttonPanel.setLeaveButtonEnable(false);
	}
	
	public void unlockLeaveButton() {
		buttonPanel.setLeaveButtonEnable(true);
	}
	private void uiEvents() {
		lobbyListPanel.addSelectionListener((e) -> {
			refreshInfoPanel();
		});
	
}}
