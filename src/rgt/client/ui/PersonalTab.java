package rgt.client.ui;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;

import engine.graphics.Camera;
import engine.graphics.Renderer;
import engine.graphics.StretchedViewport;
import rgt.client.ClientInfo;
import rgt.ui.common.Tab;

public class PersonalTab extends Tab {

	private ClientInfo info;

	private PersonalInfoPanel personalListPanel;
	
	private Color gold;
	private Color silver;
	private Color bronze;

	public PersonalTab(ClientInfo info) {
		this.info = info;
		
		setOpaque(false);

		personalListPanel = new PersonalInfoPanel();
		personalListPanel.setBorder(BorderFactory.createTitledBorder("personal"));
		
		graphCanvas = new Canvas();
		
		gold 	= new Color(255 / 255f, 223 / 255f, 0);
		silver 	= new Color(192 / 255f, 192 / 255f, 192 / 255f);
		bronze 	= new Color(160 / 255f, 100 / 255f, 40 / 255f);
		
		uiLayout();
	}

	private void uiLayout() {
		setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(5, 5, 5, 5);
		constraints.fill = GridBagConstraints.VERTICAL;
		constraints.anchor = GridBagConstraints.NORTH;
		constraints.weighty = 1.0;

		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(personalListPanel, constraints);
		constraints.fill = GridBagConstraints.NONE;

		constraints.gridx = 1;
		constraints.gridy = 0;
		constraints.anchor = GridBagConstraints.NORTH;
		constraints.weightx = 1.0;
		
		constraints.fill = GridBagConstraints.BOTH;
		graphCanvas.setPreferredSize(new Dimension(300, 200));
		
		add(graphCanvas, constraints);
	}

	private Renderer renderer;
	private Camera camera;
	private StretchedViewport viewport;
	private Canvas graphCanvas;
	
	private int virtualWidth;
	private int virtualHeight;
	
	@Override
	public void enter() {
		virtualWidth = 500;
		virtualHeight = 500;
		
		camera = new Camera();
		camera.lookAtNow(virtualWidth / 2, virtualHeight / 2);
		
		viewport = new StretchedViewport(virtualWidth, virtualHeight, true);
		viewport.setCamera(camera);
		viewport.update(graphCanvas.getWidth(), graphCanvas.getHeight());
		
		renderer = new Renderer(viewport);
		renderer.setCanvas(graphCanvas);
		
		counter = 0;
		
		
		personalListPanel.setInfo(info.gamesPlayed, info.firstPlaces, info.secondPlaces, info.thirdPlaces);
		
		if (graphCanvas.getBufferStrategy() == null) {
			graphCanvas.createBufferStrategy(3);
		}
	}
	
	private int counter;
	private int maxCounter = 60;
	
	@Override
	public void update() {
		if(counter < maxCounter) {
			counter+=1;
		}
	}
	
	@Override
	public void draw() {
		camera.update();
		
		viewport.update(graphCanvas.getWidth(), graphCanvas.getHeight());	
		
		renderer.begin();
		
		boolean noWins = (info.firstPlaces == 0 && info.secondPlaces == 0 && info.thirdPlaces == 0);
		Color backGroundColor = noWins ? Color.white : Color.darkGray; 
		renderer.clear(backGroundColor);

		drawBarPlots();
		
		renderer.end();
	}

	@Override
	public void leave() {
	}
	
	public void drawBarPlots() {
		/*
		int _1st = 6;
		int _2nd = 1;
		int _3rd = 12;
		*/
		
		int _1st = info.firstPlaces;
		int _2nd = info.secondPlaces;
		int _3rd = info.thirdPlaces;
		
		int totalNumberOfWins = _1st + _2nd + _3rd;
		if (totalNumberOfWins == 0)	return;
		
		float ratio = counter / (float) maxCounter;
		float goldHeight = _1st / (float) totalNumberOfWins * virtualHeight * ratio;
		float silverHeight = _2nd / (float) totalNumberOfWins * virtualHeight * ratio;
		float bronzeHeight = _3rd / (float) totalNumberOfWins * virtualHeight * ratio;
		
		float barWidth = virtualWidth / 13f * 3f;
		int numberOfBarPlots = 3;
		int numberOfGaps = numberOfBarPlots + 1;

		float gap = (virtualWidth - numberOfBarPlots * barWidth) / (float) numberOfGaps;

		renderer.setColor(gold);
		renderer.fillRect(gap, 0, barWidth, goldHeight);
		
		renderer.setColor(silver);
		renderer.fillRect(gap*2+barWidth, 0, barWidth, silverHeight);

		renderer.setColor(bronze);
		renderer.fillRect(3*gap+2*barWidth, 0, barWidth, bronzeHeight);
	}
}
