package rgt.client.ui;

import javax.swing.JLabel;

import rgt.ui.common.RowPanel;

public class PersonalInfoPanel extends RowPanel {

	private JLabel totalGamesField;
	private JLabel firstPlaceField;
	private JLabel secondPlaceField;
	private JLabel thirdPlaceField;

	public PersonalInfoPanel() {
		this.totalGamesField = new JLabel();
		this.firstPlaceField = new JLabel();
		this.secondPlaceField = new JLabel();
		this.thirdPlaceField = new JLabel();
		
		addFieldRow(new JLabel("Total Games Played:"), totalGamesField);
		addFieldRow(new JLabel("1st Places:"), firstPlaceField);
		addFieldRow(new JLabel("2nd Places:"), secondPlaceField);
		addFieldRow(new JLabel("3rd Places:"), thirdPlaceField);
	}

	public void setInfo(int totalGames, int first, int second, int third) {
		totalGamesField.setText(totalGames + "");
		firstPlaceField.setText(first  + "");
		secondPlaceField.setText(second + "");
		thirdPlaceField.setText(third + "");
	}
}