package rgt.client.ui;

import java.util.HashMap;
import java.util.Map;

import javax.swing.JLabel;

import rgt.ui.common.RowPanel;

public class LobbyPanel extends RowPanel  {

	private JLabel lobbyNameField;
	
	private Map<Integer, JLabel> playerNameFields;
	private int numberOfPlayerFields;

	public LobbyPanel() {
		lobbyNameField = new JLabel();
		
		numberOfPlayerFields = 6;
		
		playerNameFields = new HashMap<>();
		
		for (int i = 0; i < numberOfPlayerFields; i++) {
			playerNameFields.put(i, new JLabel());
		}
		
		for (int i = 0; i < numberOfPlayerFields; i++) {
			String text = String.format("player%d:", i);
			addFieldRow(new JLabel(text), playerNameFields.get(i));
		}
	}

	public void setLobbyName(String name) {
		lobbyNameField.setText(name);
	}
	
	public void setPlayerNameField(int index, String text) {
		JLabel playerNameField = playerNameFields.get(index);
		playerNameField.setText(text);
	}
	
	public int getNumberOfPlayerFields() {
		return numberOfPlayerFields;
	}
	
	public void clearPlayerNameFields() {
		for (int i = 0; i < numberOfPlayerFields; i++) {
			setPlayerNameField(i, "");
		}
	}
}
