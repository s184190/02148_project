package rgt.client.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;

import engine.Utils;
import rgt.ui.common.HintetPasswordField;
import rgt.ui.common.HintetTextField;

public class SignInPanel extends JPanel {

	private Color contentColor;

	private JButton signInButton;
	private JButton createAccountButton;

	private HintetTextField usernameTextField;
	private HintetPasswordField passwordTextField;
	private HintetTextField ipTextField;

	private JPanel content;

	public SignInPanel() {
		content = new JPanel();

		signInButton = new JButton("Sign in");
		createAccountButton = new JButton("Create account");

		usernameTextField = new HintetTextField("Username");
		passwordTextField = new HintetPasswordField("Password");
		ipTextField = new HintetTextField("IP-address");
		
		contentColor = Color.white;

		initUiLayout();
	}

	private void initUiLayout() {
		content.setBackground(contentColor);
		content.setBorder(createContentBorder());
		content.setLayout(new GridBagLayout());

		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(5, 5, 5, 5);
		constraints.fill = GridBagConstraints.BOTH;
		
		constraints.gridx = 0;
		constraints.gridy = 0;
		BufferedImage tetrisLogo = Utils.loadImage("/tetrisLogo.png");
		JLabel tetrisLabel = new JLabel(new ImageIcon(tetrisLogo));
		content.add(tetrisLabel, constraints);

		constraints.gridx = 0;
		constraints.gridy += 1;
		usernameTextField.setPreferredSize(new Dimension(200, 30));
		content.add(usernameTextField, constraints);

		constraints.gridx = 0;
		constraints.gridy += 1;
		passwordTextField.setPreferredSize(new Dimension(200, 30));
		content.add(passwordTextField, constraints);

		constraints.gridx = 0;
		constraints.gridy += 1;
		ipTextField.setPreferredSize(new Dimension(200, 30));
		content.add(ipTextField, constraints);

		constraints.gridx = 0;
		constraints.gridy += 1;
		constraints.anchor = GridBagConstraints.CENTER;
		// constraints.insets.top = 100;
		signInButton.setPreferredSize(new Dimension(200, 30));
		content.add(signInButton, constraints);

		constraints.gridx = 0;
		constraints.gridy += 1;
		constraints.anchor = GridBagConstraints.CENTER;
		createAccountButton.setPreferredSize(new Dimension(200, 30));
		content.add(createAccountButton, constraints);

		setLayout(new GridBagLayout());
		constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.NONE;
		constraints.anchor = GridBagConstraints.CENTER;
		add(content, constraints);
	}

	private Border createContentBorder() {
		Border contentBorder = BorderFactory.createEtchedBorder(EtchedBorder.RAISED);
		Border emptyBorder = BorderFactory.createEmptyBorder(10, 10, 10, 10);
		return BorderFactory.createCompoundBorder(contentBorder, emptyBorder);
	}
	
	public void initUiEvents(ActionListener createAccountListener, ActionListener signInListener) {
		createAccountButton.addActionListener(createAccountListener);
		signInButton.addActionListener(signInListener);
	}
	
	public String getUsername() {
		return usernameTextField.getText();
	}
	
	public String getPassword() {
		return passwordTextField.getText();
	}
	public String getIP4() {
		return ipTextField.getText();
	}

	public void enableIpTextfield() {
		ipTextField.setEditable(true);
	}
	
	public void disableIpTextfield() {
		ipTextField.setEditable(false);
	}
}
