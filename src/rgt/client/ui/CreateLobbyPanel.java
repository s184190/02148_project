package rgt.client.ui;

import java.awt.event.ActionListener;

import javax.swing.JButton;

import rgt.ui.common.HintetTextField;
import rgt.ui.common.RowPanel;

public class CreateLobbyPanel extends RowPanel {

	private HintetTextField nameField;
	private JButton createButton;

	public CreateLobbyPanel() {
		nameField = new HintetTextField("name");
		createButton = new JButton("create");
		
		addRow(nameField);
		addRow(createButton);
	}

	public void onCreate(ActionListener actionListener) {
		createButton.addActionListener(actionListener);
	}

	public String getLobbyName() {
		return nameField.getText();
	}
	
	public void setCreateButtonEnabled(boolean enabled) {
		createButton.setEnabled(enabled);
	}

}
