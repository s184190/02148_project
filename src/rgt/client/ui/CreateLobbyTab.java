package rgt.client.ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;

import rgt.client.ClientInfo;
import rgt.shared.protocols.user.lobbyList.ClientLobbyInfo;
import rgt.ui.common.Tab;

public class CreateLobbyTab extends Tab {
	
	private LobbyPanel createdLobbyPanel;
	private CreateLobbyPanel createPanel;
	private LobbyOwnerPanel ownerLobbyPanel;
	
	private ClientInfo info;
	
	public CreateLobbyTab(ClientInfo info) {
		this.info = info;
		
		setOpaque(false);

		createPanel = new CreateLobbyPanel();
		createPanel.setBorder(BorderFactory.createTitledBorder("Create game"));
		
		createdLobbyPanel = new LobbyPanel();
		createdLobbyPanel.setBorder(BorderFactory.createTitledBorder("Lobby"));

		ownerLobbyPanel = new LobbyOwnerPanel();
		ownerLobbyPanel.setBorder(BorderFactory.createTitledBorder("Owner panel"));
		
		uiLayout();
	}
	
	private void uiLayout() {
		setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(5, 5, 5, 5);
		constraints.fill = GridBagConstraints.NONE;
		constraints.anchor = GridBagConstraints.NORTH;
		constraints.weighty = 1.0;

		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(createPanel, constraints);
		constraints.fill = GridBagConstraints.NONE;
		createPanel.setOpaque(false);

		constraints.gridx = 1;
		constraints.gridy = 0;
		constraints.anchor = GridBagConstraints.NORTH;
		constraints.weightx = 1.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(createdLobbyPanel, constraints);
		createdLobbyPanel.setOpaque(false);
		
		constraints.gridx = 0;
		constraints.gridy = 1;
		constraints.anchor = GridBagConstraints.WEST;
		constraints.weighty = 0.0;
		ownerLobbyPanel.setOpaque(false);
		constraints.weightx = 0.5;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		add(ownerLobbyPanel, constraints);
	}

	@Override
	public void enter() {
	}

	@Override
	public void leave() {
	}

	public void refreshInfoPanel() {
		ClientLobbyInfo lobbyInfo = info.lobbyInfo;
		
		createdLobbyPanel.setLobbyName(lobbyInfo.name);
		createdLobbyPanel.clearPlayerNameFields();
		
		for (int i = 0; i < lobbyInfo.players.size(); i++) {
			String player = lobbyInfo.players.get(i);
			createdLobbyPanel.setPlayerNameField(i, player);
		}
	}
	
	public String getLobbyName() {
		return createPanel.getLobbyName();
	}

	public void clearLobbyInfo() {
		createdLobbyPanel.setLobbyName("");
		createdLobbyPanel.clearPlayerNameFields();
	}
	
	public void setCreateListener(ActionListener actionListener) {
		createPanel.onCreate(actionListener);
	}

	public void setStartListener(ActionListener actionListener) {
		ownerLobbyPanel.onStart(actionListener);
	}
	
	public void setTerminateListener(ActionListener actionListener) {
		ownerLobbyPanel.onTerminate(actionListener);
	}
	
	public void lockTerminateButton() {
		ownerLobbyPanel.setTerminateButtonEnabled(false);
	}
	
	public void unlockTerminateButton() {
		ownerLobbyPanel.setTerminateButtonEnabled(true);
	}
	
	public void lockStartButton() {
		ownerLobbyPanel.setStartButtonEnabled(false);
	}
	
	public void unlockStartButton() {
		ownerLobbyPanel.setStartButtonEnabled(true);
	}
	
	public void lockCreateButton() {
		createPanel.setCreateButtonEnabled(false);
	}
	
	public void unlockCreateButton() {
		createPanel.setCreateButtonEnabled(true);
	}
}
