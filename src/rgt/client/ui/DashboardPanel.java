package rgt.client.ui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jspace.ActualField;
import org.jspace.FormalField;
import org.jspace.RemoteSpace;

import engine.Utils;
import engine.scenes.SceneManager;
import rgt.client.ClientInfo;
import rgt.shared.Actors;
import rgt.shared.NetworkUtil;
import rgt.shared.NonExhaustiveSwitchException;
import rgt.shared.protocols.user.broadcast.BroadcastMessages;
import rgt.shared.protocols.user.joinLobby.JoinLobbyRequest;
import rgt.shared.protocols.user.joinLobby.JoinLobbyResponse;
import rgt.shared.protocols.user.lobbyList.ClientLobbyInfo;
import rgt.shared.protocols.user.lobbyList.LobbyListResponsePacket;
import rgt.shared.protocols.user.startGame.StartGameResponse;
import rgt.shared.protocols.user.userInfo.UserInfoPacket;
import rgt.shared.protocols.user.userInfo.UserMessages;
import rgt.shared.protocols.users.lobbyCreation.LobbyCreationRequestPacket;
import rgt.shared.protocols.users.lobbyCreation.LobbyCreationResponsePacket;
import rgt.ui.common.PopUp;
import rgt.ui.common.TabPanel;

public class DashboardPanel extends TabPanel {
	
	private RemoteSpace userSpace;

	private PersonalTab personalTab;
	private CreateLobbyTab createLobbyTab;
	private FindLobbyTab findLobbyTab;
	
	private ClientInfo clientInfo;
	private List<ClientLobbyInfo> currentLobbyInfos;
	
	private SceneManager manager;
	
	public DashboardPanel(ClientInfo clientInfo, SceneManager manager) {
		super("Dashboard");
		
		this.clientInfo = clientInfo;
		this.manager = manager;
		
		currentLobbyInfos = new ArrayList<>();

		personalTab = new PersonalTab(clientInfo);
		createLobbyTab = new CreateLobbyTab(clientInfo);
		findLobbyTab = new FindLobbyTab(clientInfo, currentLobbyInfos);
		
		createLobbyTab.lockStartButton();
		createLobbyTab.lockTerminateButton();
		createLobbyTab.unlockCreateButton();
		
		createLobbyTab.setCreateListener((e) -> {
			try {
				int uid = clientInfo.uid;
				String lobbyName = createLobbyTab.getLobbyName();
				
				LobbyCreationRequestPacket lobbyRequest = new LobbyCreationRequestPacket();
				lobbyRequest.name = lobbyName;
				lobbyRequest.uid = uid;
				
				userSpace.put(Actors.server, UserMessages.createLobby, lobbyRequest);
				
				Object[] lobbyResponseTuple = userSpace.get(
						new ActualField(uid),
						new ActualField(UserMessages.createLobby),
						new FormalField(LobbyCreationResponsePacket.class));
				
				LobbyCreationResponsePacket response = (LobbyCreationResponsePacket) lobbyResponseTuple[2];
				if (response.created) {
					clientInfo.inLobby = true;
					clientInfo.lobbyInfo.lid = response.lobbyInfo.lid;
					clientInfo.lobbyInfo.name = response.lobbyInfo.name;
					clientInfo.lobbyInfo.players = response.lobbyInfo.players;
					
					lockTab(findLobbyTab);
					createLobbyTab.unlockStartButton();
					createLobbyTab.unlockTerminateButton();
					createLobbyTab.lockCreateButton();
					createLobbyTab.refreshInfoPanel();
					
					PopUp.info("success", "lobby created", this);
				} else {
					PopUp.error("failed", "could not create lobby", this);
				}
				
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		});
		
		createLobbyTab.setTerminateListener((e) -> {
				 try {
					 userSpace.put(Actors.server, UserMessages.terminateLobby, clientInfo.uid);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			});
		
		createLobbyTab.setStartListener((e) -> {
			try {
				userSpace.put(Actors.server, UserMessages.startGame, clientInfo.uid);
				
				Object[] tuple = userSpace.get(
						new ActualField(clientInfo.uid),
						new ActualField(UserMessages.startGame),
						new FormalField(StartGameResponse.class));
				
				StartGameResponse response = (StartGameResponse) tuple[2];
				
				if (response.success) {
					// PopUp.info("success", "starting game", this);
				} else {
					PopUp.error("error", "could not start game", this);
				}
				
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		});
		
		findLobbyTab.setJoinListener((e) -> {
			try {
				int uid = clientInfo.uid;
				
				ClientLobbyInfo lobbyInfo = findLobbyTab.getSelectedClientLobbyInfo();
				
				if (lobbyInfo != null) {	// nothing selected check.
					JoinLobbyRequest request = new JoinLobbyRequest();
					request.uid = uid;
					request.lid = lobbyInfo.lid;
					userSpace.put(Actors.server, UserMessages.joinLobby, request);
					
					Object[] lobbyResponseTuple = userSpace.get(
							new ActualField(uid),
							new ActualField(UserMessages.joinLobby),
							new FormalField(JoinLobbyResponse.class));
					
					JoinLobbyResponse packet = (JoinLobbyResponse) lobbyResponseTuple[2];
					
					if (packet.success) {
						lockTab(createLobbyTab);
						
						findLobbyTab.lockJoinButton();
						findLobbyTab.lockRefreshButton();
						findLobbyTab.unlockLeaveButton();
						
						clientInfo.inLobby = true;
						clientInfo.lobbyInfo = packet.info;

						findLobbyTab.refreshList();
						findLobbyTab.refreshInfoPanel();

						PopUp.info("success", "joined lobby", this);
					} else {
						PopUp.error("failed", "failed to join lobby", this);
					}
				
				}
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		});
		
		findLobbyTab.setLeaveListener((e) -> {
			try {
				int uid = clientInfo.uid;
				
				userSpace.put(Actors.server, UserMessages.leaveLobby, uid);
				
				unlockTab(createLobbyTab);
				
				findLobbyTab.lockLeaveButton();
				findLobbyTab.unlockJoinButton();
				findLobbyTab.unlockRefreshButton();
				
				clientInfo.lobbyInfo.players.clear();
				findLobbyTab.refreshInfoPanel();

			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		});
		
		findLobbyTab.setRefreshListener((e) -> {
			try {
				int uid = clientInfo.uid;
				
				userSpace.put(Actors.server, UserMessages.lobbyList, uid);
				
				Object[] lobbyResponseTuple = userSpace.get(
						new ActualField(uid),
						new ActualField(UserMessages.lobbyList),
						new FormalField(LobbyListResponsePacket.class));
				
				LobbyListResponsePacket packet = (LobbyListResponsePacket) lobbyResponseTuple[2];
				
				currentLobbyInfos.clear();
				currentLobbyInfos.addAll(packet.lobbies);
				findLobbyTab.refreshList();
				
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		});
		
		setupTabs();
	}

	private void setupTabs() {
		addTab("Personal", personalTab);
		addTab("Create Lobby", createLobbyTab);
		addTab("Find Lobby", findLobbyTab);
	}
	
	public void enter() {
		setInitialTab(personalTab);

		connectToTetrisServer();
		requestUserInfo();

		refreshAllTabContent();
		
		// call activeTab.enter(), but we first need to set the tab
		// we can not do this in setupTabs, because when a game is over
		// we jump back to this scene. "setupTabs()" is only called once (on init). 
		super.enter();	
	}

	private void refreshAllTabContent() {
		createLobbyTab.clearLobbyInfo();
		
		unlockTab(createLobbyTab);
		unlockTab(findLobbyTab);
		
		findLobbyTab.lockLeaveButton();
		findLobbyTab.unlockJoinButton();
		findLobbyTab.unlockRefreshButton();
		
		createLobbyTab.lockStartButton();
		createLobbyTab.lockTerminateButton();
		createLobbyTab.unlockCreateButton();
		
		currentLobbyInfos.clear();
		
		clientInfo.lobbyInfo.players.clear();
		findLobbyTab.refreshInfoPanel();
		findLobbyTab.refreshList();
	}

	private void requestUserInfo() {
		try {
			int uid = clientInfo.uid;
			userSpace.put(Actors.server, UserMessages.userInfo, uid);
			
			Object[] userSpaceTuple = userSpace.get(
					new ActualField(uid), 
					new ActualField(UserMessages.userInfo),
					new FormalField(UserInfoPacket.class));
			
			UserInfoPacket response = (UserInfoPacket) userSpaceTuple[2];
			
			clientInfo.uid = response.uid;
			clientInfo.username = response.username;
			clientInfo.inGame = response.inGame;
			clientInfo.gameInfo.path = response.gameSpacePath;
			
			clientInfo.gamesPlayed = response.gamesPlayed;
			clientInfo.firstPlaces = response.firstPlaces;
			clientInfo.secondPlaces = response.secondPlaces;
			clientInfo.thirdPlaces = response.thirdPlaces;

		} catch (InterruptedException e) {
		}
	}

	private void connectToTetrisServer() {
		try {
			String userGate = NetworkUtil.getGate(clientInfo.ip4, NetworkUtil.tetrisSpaceId);
			userSpace = new RemoteSpace(userGate);
		} catch (IOException e) {
			Utils.sleep(5_000);	// wait for disconnect thread to handle it
		}
	}

	@Override
	public void update() {
		super.update();

		if (clientInfo.inGame) {
			manager.changeScene("gameScene");
		}
		
		fetchBroadcastTuple();
	}

	private void fetchBroadcastTuple() {
		try {
			Object[] broadcastTuple = userSpace.getp(
					new ActualField(clientInfo.uid),
					new FormalField(BroadcastMessages.class),
					new FormalField(Object.class));
			
			if (broadcastTuple != null) {
				BroadcastMessages message = (BroadcastMessages) broadcastTuple[1];
				switch (message) {
					case start: {
						String gameSpacePath = (String) broadcastTuple[2];
						
						clientInfo.gameInfo.path = gameSpacePath;
						clientInfo.inLobby = false;
						clientInfo.inGame = true;
						
						manager.changeScene("gameScene");
						
						break;
					}
				
					case terminate: {
						clientInfo.inLobby = false;
						refreshAllTabContent();

						PopUp.info("lobby gone", "you were removed from the lobby", this);
						
						break;
					}
					
					case join: {
						String leavingPlayer = (String) broadcastTuple[2];
						clientInfo.lobbyInfo.players.add(leavingPlayer);
	
						PopUp.info("join", String.format("'%s' joined", leavingPlayer), this);
	
						createLobbyTab.refreshInfoPanel();
						findLobbyTab.refreshInfoPanel();
						
						break;
					}
					
					case leave: {
						String leavingPlayer = (String) broadcastTuple[2];
						clientInfo.lobbyInfo.players.remove(leavingPlayer);
	
						PopUp.info("leave", String.format("'%s' left", leavingPlayer), this);
	
						createLobbyTab.refreshInfoPanel();
						findLobbyTab.refreshInfoPanel();
						
						break;
					}
				
				default: 
					throw new NonExhaustiveSwitchException();
				}
			}
		} catch (InterruptedException e) {
			// e.printStackTrace();
			// handled by alive thread.
		}
	}
	
	private void closeConnections() {
		if (clientInfo.connected) {
			if (clientInfo.loggedIn) {
				try {
					userSpace.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public void dispose() {
		closeConnections();
	}
}
