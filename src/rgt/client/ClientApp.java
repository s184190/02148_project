package rgt.client;

import engine.ApplicationListener;
import engine.scenes.SceneManager;
import rgt.client.scenes.DashboardScene;
import rgt.client.scenes.GameScene;
import rgt.client.scenes.SignInScene;

public class ClientApp extends ApplicationListener {
	
	private SceneManager manager;
	
	@Override
	public void init() {
		app.setTitle("tetris client");
		
		manager = new SceneManager(app, input);
		
		ClientInfo info = new ClientInfo();

		manager.addScene("signinScene", new SignInScene(info));
		manager.addScene("dashboardScene", new DashboardScene(info));
		manager.addScene("gameScene", new GameScene(info));
		
		manager.changeScene("signinScene");
		//manager.changeScene("dashboardScene");
		// manager.changeScene("gameScene");
		
	}

	@Override
	public void update() {
		manager.update();
	}

	@Override
	public void draw() {
		manager.draw();
	}

	@Override
	public void dispose() {
		manager.dispose();
	}

	@Override
	public void resize(int width, int height) {
		manager.resize(width, height);
	}
}
