package rgt.client.scenes;

import engine.scenes.Scene;
import rgt.client.ClientInfo;
import rgt.client.ui.DashboardPanel;

public class DashboardScene extends Scene {

	private ClientInfo clientInfo;
	private DashboardPanel panel;

	public DashboardScene(ClientInfo clientInfo) {
		this.clientInfo = clientInfo;
	}

	@Override
	public void init() {
		panel = new DashboardPanel(clientInfo, manager);
	}

	@Override
	public void enter() {
		app.setRoot(panel);

		panel.enter();
		panel.setSideTitle("logged in:", clientInfo.username);
	}

	@Override
	public void leave() {
		panel.leave();
	}

	@Override
	public void dispose() {
		panel.dispose();
	}

	@Override
	public void draw() {
		panel.draw();
	}

	@Override
	public void update() {
		panel.update();
	}
}
