
package rgt.client.scenes;

import java.awt.event.ActionListener;
import java.io.IOException;

import org.jspace.ActualField;
import org.jspace.FormalField;
import org.jspace.RemoteSpace;

import engine.scenes.Scene;
import rgt.client.ClientInfo;
import rgt.client.ui.SignInPanel;
import rgt.encryption.Encryption;
import rgt.server.FixedLoopProcess;
import rgt.shared.Actors;
import rgt.shared.NetworkUtil;
import rgt.shared.protocols.alive.AliveMessageTypes;
import rgt.shared.protocols.authentication.AuthenticationMessage;
import rgt.shared.protocols.authentication.Credentials;
import rgt.shared.protocols.authentication.signIn.SignInResponsePacket;
import rgt.shared.protocols.authentication.signIn.SignInStatus;
import rgt.shared.protocols.authentication.signUp.SignUpResponsePacket;
import rgt.shared.protocols.authentication.signUp.SignUpStatus;
import rgt.ui.common.PopUp;

public class SignInScene extends Scene {
	
	private RemoteSpace authenticationSpace;
	private RemoteSpace aliveSpace;
	
	private SignInPanel panel;
	
	private ClientInfo clientInfo;
	
	private Encryption encryption;
	
	public SignInScene(ClientInfo clientInfo) {
		this.clientInfo = clientInfo;
	}
	
	@Override
	public void init() {
		encryption = new Encryption();
		panel = new SignInPanel();

		clientInfo.connected = false;
		
		clientInfo.myPublicKey = "public key";
		clientInfo.myPrivateKey = "private key";

		String serverPublicKey = "private key";
		clientInfo.serverPublicKey = serverPublicKey;
	}

	@Override
	public void enter() {
		app.setRoot(panel);

		ActionListener createAccountListener = (event) -> {
			if (!clientInfo.connected) {
				tryToConnect();
			}
			
			String username = panel.getUsername();
			String password = panel.getPassword();
			
			tryToSignup(username, password);
		};
		
		ActionListener loginListener = (event) -> {
			if (!clientInfo.connected) {
				tryToConnect();
			}
			
			String requestedUsername = panel.getUsername();
			String requestedPassword = panel.getPassword();
			
			tryToSignIn(requestedUsername, requestedPassword);
		};
		
		panel.initUiEvents(createAccountListener, loginListener);
	}

	private void tryToSignIn(String requestedUsername, String requestedPassword) {
		SignInResponsePacket signInResult =	requestSignIn(requestedUsername, requestedPassword);
		
		SignInStatus status = signInResult.status;
		
		if (status == SignInStatus.success) {
			clientInfo.loggedIn = true;
			
			int uid = signInResult.uid;
			clientInfo.uid = uid;
			
			String aliveGate = NetworkUtil.getGate(clientInfo.ip4, NetworkUtil.aliveSpaceId);
			try {
				aliveSpace = new RemoteSpace(aliveGate);
			} catch (IOException e1) {
				disconnect();
			}
			
			startAliveDialog();
		
			manager.changeScene("dashboardScene");
			
		} else {
			if (status == SignInStatus.userNotFound) {
				PopUp.error("error", "wrong username or password", panel);
			} else if (status == SignInStatus.alreadyLoggedIn) {
				PopUp.error("error", "already logged in", panel);
			} else if (status == SignInStatus.disconnected) {
				disconnect();
			} else if (status == SignInStatus.notConnected){
				notConnected();
			}
		}
	}

	private void startAliveDialog() {
		new Thread(() -> {
			FixedLoopProcess process = new FixedLoopProcess() {
				
				@Override
				public void update(float dt) {
					try {
						Object[] aliveTuple = aliveSpace.getp(
								new ActualField(AliveMessageTypes.aliveRequest), 
								new ActualField(clientInfo.uid));
					
						aliveSpace.put(AliveMessageTypes.aliveResponse, clientInfo.uid);

					} catch (InterruptedException e) {
						stop();
						disconnect();
					}
				}
			};
			
			process.setTps(10);
			process.startLoop();
			
		}).start();
	}

	private SignInResponsePacket requestSignIn(String username, String password) {
		SignInResponsePacket result = new SignInResponsePacket();
		
		try {
			Credentials credentials = new Credentials(username, password);
			
			authenticationSpace.put(Actors.server, AuthenticationMessage.signIn, clientInfo.myPublicKey, credentials);
			
			Object[] responseTuple = authenticationSpace.get(
					new ActualField(credentials), 
					new ActualField(AuthenticationMessage.signIn),
					new FormalField(SignInResponsePacket.class));
			
			SignInResponsePacket response = (SignInResponsePacket) responseTuple[2];
			
			result.status = response.status;
			result.uid = response.uid;
			
			clientInfo.connected = true;

		} catch (InterruptedException e) {
			if (clientInfo.connected) {
				result.status = SignInStatus.disconnected;
			} else {
				result.status = SignInStatus.notConnected;
			}
		}
		
		return result;
	}

	private void notConnected() {
		panel.enableIpTextfield();
		PopUp.error("error", "not connected", panel);
	}

	private void tryToSignup(String username, String password) {
		SignUpStatus status;
		try {
			
			Credentials credentials = new Credentials(username, password);
			Credentials serverEncryptedCredentials = encryption.encrypt(credentials, clientInfo.serverPublicKey);
			
			authenticationSpace.put(Actors.server, AuthenticationMessage.signUp, clientInfo.myPublicKey, serverEncryptedCredentials);
			
			Credentials clientEncryptedCredentials = encryption.encrypt(credentials, clientInfo.myPublicKey);
			
			Object[] signUpTuple = authenticationSpace.get(
					new ActualField(clientEncryptedCredentials), 
					new ActualField(AuthenticationMessage.signUp), 
					new FormalField(SignUpResponsePacket.class));

			SignUpResponsePacket result = (SignUpResponsePacket) signUpTuple[2];
			status = result.status;
			
			clientInfo.connected = true;

		} catch (InterruptedException e) {
			
			if (clientInfo.connected) {
				status = SignUpStatus.disconnected;
			} else {
				status = SignUpStatus.notConnected;
			}
		}
		
		if (status == SignUpStatus.success) {
			PopUp.info("success", "succesfully created an acount", panel);
		} else if (status == SignUpStatus.weakPassword) {
			PopUp.error("error", "Too weak password", panel);
		}	else if (status == SignUpStatus.unavailableUsername) {
			PopUp.error("error", "username unavailable", panel);
		}	else if (status == SignUpStatus.notConnected){
			notConnected();
		} else {
			disconnect();
		}
	}

	private void tryToConnect() {
		String ip4 = panel.getIP4();
		
		if (NetworkUtil.useLocalHost) {
			ip4 = NetworkUtil.localhost;
		}
		
		clientInfo.ip4 = ip4;
		
		String authenticationGate = NetworkUtil.getGate(clientInfo.ip4, NetworkUtil.authenticationSpaceId);
	
		try {
			authenticationSpace = new RemoteSpace(authenticationGate);
			panel.disableIpTextfield();
		} catch (Exception e) {
			notConnected();
		}
	}

	@Override
	public void dispose() {
		if (clientInfo.connected) {
			if (clientInfo.loggedIn) {

				try {
					authenticationSpace.put(Actors.server, AuthenticationMessage.signOut, clientInfo.myPublicKey, clientInfo.uid);
					authenticationSpace.close();
					aliveSpace.close();
				} catch (InterruptedException | IOException e) {
					clientInfo.connected = false;
				}
			}
		}
	}
	
	private void disconnect() {
		PopUp.error("error", "disconnected", panel);
		clientInfo.connected = false;
		app.exit();
	}
}
