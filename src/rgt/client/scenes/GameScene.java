package rgt.client.scenes;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jspace.ActualField;
import org.jspace.FormalField;
import org.jspace.RemoteSpace;

import engine.graphics.Camera;
import engine.graphics.FitViewport;
import engine.graphics.Renderer;
import engine.input.Keys;
import engine.math.Vector2;
import engine.scenes.Scene;
import rgt.client.ClientInfo;
import rgt.shared.Actors;
import rgt.shared.NetworkUtil;
import rgt.shared.NonExhaustiveSwitchException;
import rgt.shared.protocols.game.ClientGameMessages;
import rgt.shared.protocols.game.GameMetaMessage;
import rgt.shared.protocols.game.ServerGameMessages;
import rgt.tetris.networking.Assets;
import rgt.tetris.networking.Board;
import rgt.tetris.networking.BoardLayout;
import rgt.tetris.networking.GameLayout;
import rgt.tetris.networking.Orientation;
import rgt.tetris.networking.Piece;
import rgt.tetris.networking.Point;
import rgt.tetris.networking.Tetris;
import rgt.ui.common.PopUp;

public class GameScene extends Scene {
	
	private ClientInfo info;
	
	private RemoteSpace gameSpace;
	
	private Tetris tetris;
	
	private Renderer renderer;
	private FitViewport viewport;
	private Camera camera;
	private	Assets assets;
	private GameLayout gameLayout;
	
	private Canvas canvas;
	
	private int boardId;
	
	private float activateTime;
	private float repeatTime;
	
	private RepeatingKey down;
	private RepeatingKey left;
	private RepeatingKey right;
	
	public GameScene(ClientInfo info) {
		this.info = info;
	}

	@Override
	public void init() {
		assets = new Assets();
		tetris = new Tetris();
		
		activateTime = 0.25f;
		repeatTime = 0.03f;
		
		down = new RepeatingKey(activateTime, repeatTime, Keys.DOWN, input);
		left = new RepeatingKey(activateTime, repeatTime, Keys.LEFT, input);
		right = new RepeatingKey(activateTime, repeatTime, Keys.RIGHT, input);
	}
	
	@Override
	public void enter() {
		try {
			String gate = NetworkUtil.getGate(info.ip4, info.gameInfo.path);
			gameSpace = new RemoteSpace(gate);

			// consume all old messages (moves) in case of a reconnect.
			getAllGameMessages();

			gameSpace.put(Actors.gameServer, GameMetaMessage.gameState, info.uid);
			
			Object[] gameStateTuple = gameSpace.get(
					new ActualField(info.uid),
					new ActualField(GameMetaMessage.gameState),
					new FormalField(GameState.class));
			
			GameState gameState = (GameState) gameStateTuple[2];
			
			boardId = gameState.yourBoardId;
			List<Board> boards = gameState.boards;
			tetris.populate(boards);
		} catch (IOException | InterruptedException e) {
			disconnect();
		}
		
		int boardWidth = tetris.getWidth();
		int boardHeight = tetris.getHeight();
		int numberOfBoards = tetris.getNumberOfBoards();
		
		gameLayout = new GameLayout(boardId, numberOfBoards, boardWidth, boardHeight);
		
		canvas = new Canvas();
		
		app.setRoot(canvas);
		app.attachListener(canvas);

		canvas.createBufferStrategy(3);
		
		camera = new Camera();
		camera.zoomNow(0.5f);
		camera.zoom(1f);
		
		Vector2 layoutDimensions = gameLayout.getLayoutDimensions();
		
		float cameraPaddingX = 6 * gameLayout.getMainBoardTilesize();
		float cameraPaddingY = gameLayout.getMainBoardTilesize();
		float virtualWidth = layoutDimensions.x + cameraPaddingX * 2;
		float virtualHeight = layoutDimensions.y + cameraPaddingY * 2;
		
		float virtualCenterX = virtualWidth / 2f;
		float virtualCenterY = virtualHeight / 2f;
		
		float cameraX = virtualCenterX - (virtualCenterX - layoutDimensions.x / 2f);
		float cameraY = virtualCenterY - (virtualCenterY - layoutDimensions.y / 2f);
		
		camera.lookAtNow(cameraX, cameraY);
		viewport = new FitViewport(virtualWidth, virtualHeight, true);
		viewport.setCamera(camera);

		renderer = new Renderer(viewport);
		renderer.setCanvas(canvas);

		app.attachListener(canvas);

		viewport.update(app.getWidth(), app.getHeight());
		
		input.releaseAllKeys();
	}
	
	@Override
	public void update() {	
		float dt = app.getDelta();
		down.update(dt);
		left.update(dt);
		right.update(dt);
		
		if (down.repeated()) requestMove(ClientGameMessages.down);
		if (left.repeated()) requestMove(ClientGameMessages.left);
		if (right.repeated()) requestMove(ClientGameMessages.right);

		if (input.isKeyJustPressed(Keys.LEFT)) requestMove(ClientGameMessages.left);
		if (input.isKeyJustPressed(Keys.RIGHT)) requestMove(ClientGameMessages.right);
		if (input.isKeyJustPressed(Keys.DOWN)) requestMove(ClientGameMessages.down);
		if (input.isKeyJustPressed(Keys.UP)) requestMove(ClientGameMessages.rotate);
		if (input.isKeyJustPressed(Keys.SPACE)) requestMove(ClientGameMessages.space);
		if (input.isKeyJustPressed(Keys.C)) requestMove(ClientGameMessages.hold);
		
		List<Object[]> tuples = getAllGameMessages();
		for (Object[] tuple : tuples) {
			ServerGameMessages message = (ServerGameMessages) tuple[1];
			Object packet = tuple[2];
			
			switch (message) {
				case fall: {
					int boardIndex = (int) packet;
					Board board = tetris.getBoard(boardIndex);
					tetris.fall(board);
					break;
				}
				
				case left: {
					int boardIndex = (int) packet;
					Board board = tetris.getBoard(boardIndex);
					tetris.left(board);
					break;
				}
				
				case right: {
					int boardIndex = (int) packet;
					Board board = tetris.getBoard(boardIndex);
					tetris.right(board);
					break;
				}
				
				case rotate: {
					int boardIndex = (int) packet;
					Board board = tetris.getBoard(boardIndex);
					tetris.rotate(board);
					break;
				}
				
				case down: {
					int boardIndex = (int) packet;
					Board board = tetris.getBoard(boardIndex);
					tetris.down(board);
					break;
				}
				
				case space: {
					int boardIndex = (int) packet;
					Board board = tetris.getBoard(boardIndex);
					tetris.space(board);
					break;
				}
				
				case hold: {
					int boardIndex = (int) packet;
					Board board = tetris.getBoard(boardIndex);
					tetris.hold(board);
					break;
				}
				
				case solidify: {
					int boardIndex = (int) packet;
					Board board = tetris.getBoard(boardIndex);
					tetris.onSolidify(board);
					break;
				}
				
				case gameOver: {
					info.inGame = false;
					manager.changeScene("dashboardScene");
					break;
				}
				
				
				default: 
					throw new NonExhaustiveSwitchException("unsupported: " + message);
			}
		}
	}
	
	private void requestMove(ClientGameMessages message) {
		try {
			gameSpace.put(Actors.gameServer, message, info.uid);
		} catch (InterruptedException e) {
			// e.printStackTrace();
		}
	}

	private List<Object[]> getAllGameMessages() {
		List<Object[]> tuples = new ArrayList<Object[]>();
		
		try {
			tuples = gameSpace.getAll(
					new ActualField(info.uid),
					new FormalField(ServerGameMessages.class),
					new FormalField(Object.class));
		} catch (InterruptedException e) {
			// e.printStackTrace();
		}
		
		return tuples;
	}
	
	private void disconnect() {
		PopUp.error("error", "disconnected", canvas);
		info.connected = false;
		app.exit();
	}

	@Override
	public void draw() {
		camera.update();
		
		renderer.begin();
		renderer.clear(Color.black);
		
		for (BoardLayout layout : gameLayout.getSubBoardLayouts()) {
			Board board = tetris.getBoard(layout.boardIndex);
			drawSubBoard(layout.x, layout.y, layout.tilesize, board);
		}
		
		BoardLayout mainLayout = gameLayout.getMainBoardLayout();
		Board mainBoard = tetris.getBoard(mainLayout.boardIndex);
		drawMainBoard(mainLayout.x, mainLayout.y, mainLayout.tilesize, mainBoard);

		renderer.end();
	}
	
	private void drawMainBoard(float offsetX, float offsetY, float tilesize, Board board) {
		
		drawGrid(offsetX, offsetY, tilesize, board);

		ArrayList<Point> piecePoints = tetris.pieces.get(board.piece, board.orientation);
		drawPiece(piecePoints, board.piece, offsetX, offsetY, tilesize, board);
		
		Point ghostPosition = tetris.getGhostPosition(board);
		drawPiece(piecePoints, board.piece, ghostPosition, offsetX, offsetY, tilesize, 0.4f, board);

		// holding piece
		if (board.holdingPiece != null) {
			ArrayList<Point> holdingPiecePoints = tetris.pieces.get(board.holdingPiece, rgt.tetris.networking.Orientation._0);
			
			int maxPieceLength = tetris.maxPieceLength();
			int holdingGridX = -maxPieceLength;
			int holdingGridY = board.looseHeight - maxPieceLength;
			Point holdingPosition = new Point(holdingGridX, holdingGridY);
			
			float holdingTilesize = tilesize;
			Piece holdingPiece = board.holdingPiece;
			
			drawPiece(
					holdingPiecePoints, 
					holdingPiece, 
					holdingPosition, 
					offsetX - tilesize / 2, 
					offsetY, 
					holdingTilesize, 
					1f, 
					board);
		}
		
		// draw next pieces
		for (int i = 0; i < board.nextPieces.size(); i++) {
			Piece piece = board.nextPieces.get(i);
			ArrayList<Point> nextPiecePoints = tetris.pieces.get(piece, Orientation._0);
			
			int maxPieceLength = tetris.maxPieceLength();
			int nextPieceGridX = board.width;
			int nextPieceGridY = board.looseHeight - maxPieceLength - maxPieceLength * i;
			Point nextPiecePosition = new Point(nextPieceGridX, nextPieceGridY);
			
			float nextPieceTilesize = tilesize;

			drawPiece(nextPiecePoints, piece, nextPiecePosition, offsetX + tilesize / 2, offsetY, nextPieceTilesize, 1f, board);
		}

		
		drawDebtMeter(offsetX, offsetY, tilesize, board);
		drawOverrideBox(offsetX, offsetY, tilesize, board);
		drawGridOutline(offsetX, offsetY, tilesize, board);
	}

	private void drawSubBoard(float offsetX, float offsetY, float tilesize, Board board) {
		
		drawGrid(offsetX, offsetY, tilesize, board);

		ArrayList<Point> piecePoints = tetris.pieces.get(board.piece, board.orientation);
		drawPiece(piecePoints, board.piece, offsetX, offsetY, tilesize, board);
		
		Point ghostPosition = tetris.getGhostPosition(board);
		drawPiece(piecePoints, board.piece, ghostPosition, offsetX, offsetY, tilesize, 0.4f, board);

		drawDebtMeter(offsetX, offsetY, tilesize, board);
		drawOverrideBox(offsetX, offsetY, tilesize, board);
		drawGridOutline(offsetX, offsetY, tilesize, board);
	}
	
	private void drawPiece(List<Point> piecePoints, Piece piece, Point position, float offsetX, float offsetY, float tilesize, float opacity, Board board) {
		for (Point point : piecePoints) {
			float x = (point.x + position.x) * tilesize + offsetX;
			float y = (point.y + position.y) * tilesize + offsetY;
			float width = tilesize;
			float height = tilesize;

			int brick = tetris.pieces.pieceToBrick(piece);
			BufferedImage img = assets.getBrick(brick);
			renderer.drawImage(img, x, y, width, height, opacity);
		}
	}
	
	private void drawPiece(List<Point> piecePoints, Piece piece, float offsetX, float offsetY, float tilesize, Board board) {
		drawPiece(piecePoints, piece, board.position, offsetX, offsetY, tilesize, 1f, board);
	}
	
	private void drawOverrideBox(float offsetX, float offsetY, float tilesize, Board board) {
		float yy = board.looseHeight * tilesize;
		
		float ww = board.width * tilesize;
		float hh = (board.height - board.looseHeight) * tilesize;
		
		renderer.setColor(Color.black);
		renderer.fillRect(0 + offsetX, yy + offsetY, ww, hh);
	}

	private void drawGridOutline(float offsetX, float offsetY, float tilesize, Board board) {
		float x = offsetX;
		float y = offsetY;
		float width = board.width * tilesize;
		float height = board.looseHeight * tilesize;
	
		float border = 2f;
		drawBorderBox(x, y, width, height, border, Color.white);
	}

	private void drawDebtMeter(float offsetX, float offsetY, float tilesize, Board board) {
		float debtPercentage = board.totalDebt / (float) board.height;
		
		float x = offsetX + board.width * tilesize;
		float y = offsetY;
		
		float width = 6;
		float height = debtPercentage * board.height * tilesize;
		
		renderer.setColor(Color.red);
		renderer.fillRect(x, y, width, height);
	}

	private void drawGrid(float offsetX, float offsetY, float tilesize, Board board) {
		for (int x = 0; x < board.width; x++) {
			for (int y = 0; y < board.height; y++) {
				float xx = x * tilesize + offsetX;
				float yy = y * tilesize + offsetY;
				float width = tilesize;
				float height = tilesize;

				int block = board.grid[x][y];
				if (block == 0) continue;
				
				BufferedImage img = assets.getBrick(block);
				renderer.drawImage(img, xx, yy, width, height);
			}
		}
	}
	
	private void drawBorderBox(float x, float y, float width, float height, float border, Color borderColor) {
		
		renderer.setColor(borderColor);

		{	// left
			float xx = x;
			float yy = y;
			float wwidth = border;
			float hheight = height;
		
			renderer.fillRect(xx, yy, wwidth, hheight);
		}
		
		{	// right
			float xx = x + width - border;
			float yy = y;
			float wwidth = border;
			float hheight = height;
			renderer.fillRect(xx, yy, wwidth, hheight);
		}
		
		{	// top
			float xx = x;
			float yy = y + height - border;
			float wwidth = width;
			float hheight = border;
			renderer.fillRect(xx, yy, wwidth, hheight);
		}
		
		{	// bot
			float xx = x;
			float yy = y;
			float wwidth = width;
			float hheight = border;
			renderer.fillRect(xx, yy, wwidth, hheight);
		}
		
	}
	
	@Override
	public void resize(int width, int height) {
		viewport.update(width, height);
	}
	
	
}
