package rgt.client.scenes;

import engine.input.Input;

public class RepeatingKey {
	
	private float timeToActivate;
	private float activateTimer;
	
	private float timeToRepeat;
	private float repeatTimer;
	
	private boolean repeating;
	private boolean activate;
	
	private boolean fire;
	
	private int key;
	
	private Input input;
	
	public RepeatingKey(float timeToActivate, float timeToRepeat, int key, Input input) {
		this.timeToActivate = timeToActivate;
		this.timeToRepeat = timeToRepeat;
		this.key = key;
		this.input = input;
		
		reset();
	}
	
	public void reset() {
		repeating = false;
		activate = false;
		
		fire = false;
		
		repeatTimer = 0;
		activateTimer = 0;
	}
	
	public void update(float dt) {
		fire = false;
		
		if (input.isKeyJustPressed(key)) {
			activate = true;
		}
		
		if (input.isKeyReleased(key)) {
			reset();
		}
		
		if (activate) {
			activateTimer += dt;
			
			if (activateTimer >= timeToActivate) {
				activate = false;
				repeating = true;
			}
		}
		
		if (repeating) {
			repeatTimer += dt;
			
			if (repeatTimer >= timeToRepeat) {
				repeatTimer -= timeToRepeat;
				fire = true;
			}
		}
	}
	
	public boolean repeated() {
		return fire;
	}
}
