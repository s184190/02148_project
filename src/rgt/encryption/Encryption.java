package rgt.encryption;

import rgt.shared.protocols.authentication.Credentials;

public class Encryption {
	
	public Credentials encrypt(Credentials credentials, String publicKey) {
		return new Credentials(credentials.username, credentials.password);
	}
	
	public Credentials decrypt(Credentials credentials, String publicKey) {
		return new Credentials(credentials.username, credentials.password);
	}

}
