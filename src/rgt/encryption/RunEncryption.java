package rgt.encryption;

import java.nio.charset.Charset;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;

import engine.Utils;

public class RunEncryption {

	public static void main(String[] args) throws Exception {
		Encryption encryption = new Encryption();
		KeyPair keyPair = encryption.genererateKeyPair();
			
		String publicKey = new String(keyPair.getPublic().getEncoded());
		String privateKey = new String(keyPair.getPrivate().getEncoded());
		
		System.out.printf("public: \n'%s'\n", publicKey);
		System.out.printf("private: \n'%s'\n", privateKey);
		
		String message = "hello sailor";
		

		PrivateKey privateKey2 = encryption.getPrivate(privateKey.getBytes());
		PublicKey publicKey2 = encryption.getPublic(publicKey.getBytes());
		
		
		byte[] encryptedMessage = encryption.encryptMessage(message, privateKey2);
		
		String decrypted = encryption.decryptMessage(encryptedMessage, publicKey2);
		
		System.out.printf("decrypted: '%s'\n", decrypted);
		
		//Utils.writeToFile(publicKey, "resource/server/publicKey");
		//Utils.writeToFile(publicKey, "resource/server/privateKey");
		
		byte[] content = Utils.readFileAsByte("/server/publicKey");
		
		String c = new String(content, Charset.forName("UTF-8"));
		System.out.printf(">%s\n", c);
	}
	
	private static class Encryption {
		
		private Cipher cipher;
		private String encryption = "RSA";

		public Encryption() {
			try {
				cipher = Cipher.getInstance(encryption);
			} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
				e.printStackTrace();
			}
		}
		
		public KeyPair genererateKeyPair() {
			
			try {
				KeyPairGenerator generator = KeyPairGenerator.getInstance(encryption);
				// generator.initialize(4096);
				KeyPair keyPair = generator.generateKeyPair();
				return keyPair;
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
			throw new IllegalStateException("could not create key pair");
		}
		
		
		public byte[] encryptMessage(String message, PrivateKey key) {
			try {
				cipher.init(Cipher.ENCRYPT_MODE, key);
				return cipher.doFinal(message.getBytes("UTF-8"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			throw new IllegalStateException("broken!");
		}
		
		public String decryptMessage(byte[] message, PublicKey key) {
			try {
				cipher.init(Cipher.DECRYPT_MODE, key);
				String decryptedMessage = new String(cipher.doFinal(message), "UTF-8");
				return decryptedMessage;
			} catch (Exception e) {
				e.printStackTrace();
			}
			throw new IllegalStateException("broken!");
		}
		
		public PrivateKey getPrivate(byte[] keyBytes) throws Exception {
			PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
			KeyFactory kf = KeyFactory.getInstance("RSA");
			return kf.generatePrivate(spec);
		}
		
		private PublicKey getPublic(byte[] keyBytes) {
			X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
			try {
				KeyFactory kf = KeyFactory.getInstance("RSA");
				return kf.generatePublic(spec);
			} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
				e.printStackTrace();
				return null;
			}
		}
		
	}
}
